# Liberiser base package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/motivo/liberiser-cms.svg?style=flat-square)](https://packagist.org/packages/motivo/liberiser-cms)
[![Total Downloads](https://img.shields.io/packagist/dt/motivo/liberiser-cms.svg?style=flat-square)](https://packagist.org/packages/motivo/liberiser-cms)

## Installation

You can install the package via composer:

```bash
composer require motivo/liberiser-cms
```

## Usage
Run the following scripts:

1. `php artisan backpack:base:install`
2. `php artisan backpack:crud:install`
3. `php artisan liberiser:install`
    1. You will be asked to insert your sentry key for the testing or production environment.
    2. It is possible that you are being asked to override your `App\User` and `config/backpack/base`
4. `php artisan liberiser:create-user`
    1. You will be asked to insert your first name.
    2. You will be asked to insert your last name.
    3. You will be asked to insert your email which you can login with.
    4. You will be asked to insert your password that should be used.
    5. You will be asked to select which role should be assigned. 
5. You should be able to login into backpack now.

If you want to edit the views that are being used you can run the following command

`php artisan vendor:publish --tag liberiser:views`

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email dev@motivo.nl instead of using the issue tracker.

## Credits

- [Wouter van Marrum](https://github.com/wotta)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
