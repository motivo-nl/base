<?php

return [
    'name' => 'ondertekening',
    'menu_label' => 'ondertekeningen',
    'singular' => 'ondertekening',
    'plural' => 'ondertekeningen',
    'columns' => [
        'name' => 'naam',
        'signature' => 'ondertekening',
    ],
    'fields' => [
        'name' => 'naam',
        'signature' => 'ondertekening',
    ],
];