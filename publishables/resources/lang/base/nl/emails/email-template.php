<?php

return [
    'name' => 'e-mailberichten',
    'menu_label' => 'berichten',
    'singular' => 'bericht',
    'plural' => 'berichten',
    'columns' => [
        'module' => 'module',
        'label' => 'label (motivo)',
        'subject' => 'onderwerp',
        'send_to' => 'Verzenden aan',
        'sender' => 'bezoeker',
        'receiver' => 'beheerder',
        'description' => 'bij gebeurtenis',
    ],
    'fields' => [
        'type' => 'type',
        'module' => 'module',
        'label' => 'label',
        'description' => 'omschrijving',
        'variables' => 'variabelen',
        'variable' => 'variabele',
        'variable_description' => 'omschrijving',
        'subject' => 'onderwerp',
        'content' => 'bericht',
        'signature' => 'ondertekening',
        'sender_name' => 'e-mail wordt verzonden vanaf (naam)',
        'sender_address' => 'e-mail wordt verzonden vanaf (e-mail)',
        'receiver_name' => 'e-mail wordt verzonden naar (naam)',
        'receiver_address' => 'e-mail wordt verzonden naar (e-mail)',
    ],
    'type' => [
        'sender' => 'klant kan afzender instellen',
        'receiver' => 'klant kan ontvanger instellen',
    ],
    'error' => [
        'module_label_pair_exists' => 'er bestaat al een bericht voor de combinatie van de module :module en label :label',
    ],
];