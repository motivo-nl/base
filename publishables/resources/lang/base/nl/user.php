<?php

return [
    'name' => 'gebruiker',
    'menu_label' => 'gebruikers',
    'columns' => [
        'first_name' => 'voornaam',
        'last_name' => 'achternaam',
        'email' => 'e-mailadres',
        'role' => 'rol',
        'password' => 'wachwoord',
        'password_confirm' => 'bevestig wachwoord',
    ],
    'fields' => [
        'first_name' => 'voornaam',
        'last_name' => 'achternaam',
        'email' => 'e-mail',
        'role' => 'rol',
    ],
    'role' => [
        'admin' => 'admin',
        'manager' => 'beheerder',
        'content_manager' => 'contentmanager',
    ],
    'form' => [
        'submit' => 'verzenden',
    ],
];