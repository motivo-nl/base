<?php

return [
    'all' => 'alle',
    'management' => 'beheer',
    'module' => 'module',
    'modules' => 'modules',
    'close' => 'sluiten',
    'save' => 'opslaan',
    'cancel' => 'annuleren',
    'delete' => 'verwijderen',
    'variables_title' => 'variabelen gebruiken',
    'variables_description' => 'klik op een variabele om deze in te voegen',
    'tools' => 'instrumenten',
    'emails' => 'e-mails',
    'modal' => [
        'delete' => 'Weet u zeker dat u :name wilt verwijderen?',
        'delete_title' => ':name verwijderen',
    ],
];