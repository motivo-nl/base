<?php

return [
    'name' => 'e-mails',
    'menu_title' => 'e-mails',
    'menu_label' => 'verzonden',
    'singular' => 'e-mail',
    'plural' => 'e-mails',
    'index' => 'verzonden e-mails',
    'columns' => [
        'sender' => 'afzender',
        'receiver' => 'ontvanger',
        'subject' => 'onderwerp',
        'created_at' => 'verzonden op',
    ],
    'status' => [
        'title' => 'status',
        'queued' => 'in proces',
        'sent' => 'verzonden',
        'failed' => 'niet verzonden',
    ],
];