<?php

return [
    'name' => 'user',
    'menu_label' => 'users',
    'columns' => [
        'first_name' => 'first name',
        'last_name' => 'last name',
        'email' => 'e-mail',
        'role' => 'role',
        'password' => 'password',
        'password_confirm' => 'confirm password',
    ],
    'fields' => [
        'first_name' => 'first name',
        'last_name' => 'last name',
        'email' => 'e-mail',
        'role' => 'role',
    ],
    'role' => [
        'admin' => 'admin',
        'manager' => 'manager',
        'content_manager' => 'contentmanager',
    ],
    'form' => [
        'submit' => 'submit',
    ],
];