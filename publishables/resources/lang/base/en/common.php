<?php

return [
    'all' => 'all',
    'management' => 'management',
    'module' => 'module',
    'modules' => 'modules',
    'close' => 'close',
    'save' => 'save',
    'cancel' => 'cancel',
    'delete' => 'delete',
    'variables_title' => 'use variable',
    'variables_description' => 'click on a variable to insert it',
    'tools' => 'tools',
    'emails' => 'e-mails',
    'modal' => [
        'delete' => 'are you sure you want to delete :name ?',
        'delete_title' => 'delete :name',
    ],
];