<?php

return [
    'name' => 'emails',
    'menu_title' => 'emails',
    'menu_label' => 'sent emails',
    'singular' => 'email',
    'plural' => 'emails',
    'columns' => [
        'sender' => 'sender',
        'receiver' => 'receiver',
        'subject' => 'subject',
        'created_at' => 'sent at',
    ],
    'status' => [
        'title' => 'status',
        'queued' => 'in process',
        'sent' => 'sent',
        'failed' => 'not sent',
    ]
];