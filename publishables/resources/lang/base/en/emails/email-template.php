<?php

return [
    'name' => 'message',
    'menu_label' => 'message',
    'singular' => 'message',
    'plural' => 'messages',
    'columns' => [
        'module' => 'module',
        'label' => 'label (motivo)',
        'subject' => 'subject',
        'send_to' => 'send to',
        'sender' => 'visitor',
        'receiver' => 'manager',
        'description' => 'by event',
    ],
    'fields' => [
        'type' => 'type',
        'module' => 'module',
        'label' => 'label',
        'description' => 'description',
        'variables' => 'variables',
        'variable' => 'variable',
        'variable_description' => 'description',
        'subject' => 'subject',
        'content' => 'message',
        'signature' => 'signature',
        'sender_name' => 'e-mail send from (name)',
        'sender_address' => 'e-mail send from (address)',
        'receiver_name' => 'e-mail send to (name)',
        'receiver_address' => 'e-mail send to (address)',
    ],
    'type' => [
        'sender' => 'customer can edit sender',
        'receiver' => 'customer can edit receiver',
    ],
    'error' => [
        'module_label_pair_exists' => 'A combination for module :module and label :label already exists',
    ],
];