<?php

return [
    'name' => 'signature',
    'menu_label' => 'signature',
    'singular' => 'signature',
    'plural' => 'signatures',
    'columns' => [
        'name' => 'name',
        'signature' => 'signature',
    ],
    'fields' => [
        'name' => 'name',
        'signature' => 'signature',
    ],
];