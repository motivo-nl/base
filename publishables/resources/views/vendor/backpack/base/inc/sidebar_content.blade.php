<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
@foreach($adminMenuItems as $category => $menuItems)
    @if(count($menuItems['items']) > 0)
        <li class="menu-title">
            {{ ucfirst($menuItems['title']) }}
        </li>
        @foreach($menuItems['items'] as $menuItem)
            @if(! $menuItem->isTreeView())
                <li><a href="{{ $menuItem->link }}">@if($menuItem->icon)<i class="fa {{ $menuItem->icon }}"></i>@endif <span>{{ $menuItem->label }}</span></a></li>
            @else
                <li class="treeview js-expand-menu">
                    <a href="{{ $menuItem->link }}">@if($menuItem->icon)<i class="fa {{ $menuItem->icon }}"></i>@endif <span>{{ $menuItem->label }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul id="{{ $menuItem->label }}" class="treeview-menu">
                        @foreach($menuItem->getChilds() as $childItem)
                            <li><a href="{{ $childItem->link }}">@if($childItem->icon)<i class="fa {{ $childItem->icon }}"></i>@endif <span>{{ $childItem->label }}</span></a></li>
                        @endforeach
                    </ul>
                </li>
            @endif
        @endforeach
    @endif
@endforeach

@include('backpack::base.inc.js.sidebar_content')