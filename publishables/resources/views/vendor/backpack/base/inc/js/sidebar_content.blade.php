<script>
    (function() {
        let menuitems = document.getElementsByClassName('js-expand-menu');

        for (var i = 0; i < menuitems.length; i++) {
            menuitems[i].addEventListener('click', expandMenu);
        }

        function expandMenu(menu, index) {
            let redirecturl = this.firstElementChild.getAttribute('href');

            document.location.href = redirecturl;
        }
    })();
</script>