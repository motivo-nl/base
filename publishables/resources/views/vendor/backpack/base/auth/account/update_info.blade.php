@extends('backpack::layout')

@section('after_styles')
<style media="screen">
    .backpack-profile-form .required::after {
        content: ' *';
        color: red;
    }
</style>
@endsection

@section('header')
<section class="content-header">
    <h1>{{ trans('backpack::base.my_account') }}</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a>
        </li>
        <li>
            <a href="{{ route('backpack.account.info') }}">{{ trans('backpack::base.my_account') }}</a>
        </li>
        <li class="active">
            {{ trans('backpack::base.update_account_info') }}
        </li>
    </ol>
</section>
@endsection

@section('content')
<div class="row">
    <div class="col-md-3">
        @include('backpack::auth.account.sidemenu')
    </div>
    <div class="col-md-6">
        <form class="form" action="{{ route('backpack.account.info') }}" method="post">

            @csrf

            <div class="box padding-10">
                <div class="box-body backpack-profile-form">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if ($errors->count())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $e)
                                <li>{{ $e }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="required">{{ ucfirst(trans('Liberiser::user.fields.first_name')) }}</label>
                        <input required class="form-control" type="text" name="first_name" value="{{ old('first_name') ? old('first_name') : $user->first_name }}">
                    </div>
                    <div class="form-group">
                        <label class="required">{{ ucfirst(trans('Liberiser::user.fields.last_name')) }}</label>
                        <input required class="form-control" type="text" name="last_name" value="{{ old('last_name') ? old('last_name') : $user->last_name }}">
                    </div>
                    <div class="form-group">
                        <label class="required">{{ config('backpack.base.authentication_column_name') }}</label>
                        <input required class="form-control" type="{{ backpack_authentication_column()=='email'?'email':'text' }}" name="{{ backpack_authentication_column() }}" value="{{ old(backpack_authentication_column()) ? old(backpack_authentication_column()) : $user->{backpack_authentication_column()} }}">
                    </div>
                    <div class="form-group m-b-0">
                        <button type="submit" class="btn btn-success"><span class="ladda-label"><i class="fa fa-save"></i> {{ trans('backpack::base.save') }}</span></button>
                        <a href="{{ backpack_url() }}" class="btn btn-default"><span class="ladda-label">{{ trans('backpack::base.cancel') }}</span></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
