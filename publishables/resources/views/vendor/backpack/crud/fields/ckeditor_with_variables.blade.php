<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')

    <div class="row">
        <div class="col-sm-10">
            <textarea
                id="ckeditor-{{ $field['name'] }}"
                name="{{ $field['name'] }}"
                @include('crud::inc.field_attributes', ['default_class' => 'form-control'])
                >{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}</textarea>
        </div>
        <div class="col-md-2">
            <h6>{{ ucfirst(trans('Liberiser::common.variables_title')) }}</h6>
            <p class="help-block">{{ ucfirst(trans('Liberiser::common.variables_description')) }}</p>

            @foreach($field['variables'] as $variable)
                <span class="{{ $field['name'] }}_variable btn btn-primary" style="width: 100%; margin-bottom: 8px" data-variable="{{ $variable['variable'] }}">
                    {{ $variable['desc'] }}
                </span>
            @endforeach
        </div>
    </div>

    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

@if ($crud->checkIfFieldIsFirstOfItsType($field))
    @push('crud_fields_styles')
    @endpush

    @push('crud_fields_scripts')
        <script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>
    @endpush
@endif

@push('crud_fields_scripts')
<script>
    jQuery(document).ready(function($) {
        $('#ckeditor-{{ $field['name'] }}').ckeditor({
            "filebrowserBrowseUrl": "{{ url(config('backpack.base.route_prefix').'/elfinder/ckeditor') }}",
            "extraPlugins" : '{{ isset($field['extra_plugins']) ? implode(',', $field['extra_plugins']) : 'oembed,widget' }}'
            @if (isset($field['options']) && count($field['options']))
                {!! ', '.trim(json_encode($field['options']), '{}') !!}
            @endif
        });

        $('span.{{ $field['name'] }}_variable').click(function () {
            let variable = '%' + $(this).data('variable') + '%';

            CKEDITOR.instances[$('#ckeditor-{{ $field['name'] }}').attr('id')].insertText(variable);
        });
    });
</script>
@endpush
