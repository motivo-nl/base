@php
    $optionPointer = 0;
    $optionValue = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '';

    if (!isset($field['attributes']['class'])) {
        $field['attributes']['class'] = 'radio';
    }
@endphp

<div @include('crud::inc.field_wrapper_attributes') >

    <div>
        <label>{!! $field['label'] !!}</label>
        @include('crud::inc.field_translatable_icon')
    </div>

    @if( isset($field['roles']) && $field['roles'] = (array)$field['roles'] )
        @foreach ($field['roles'] as $value => $label )
            @php ($optionPointer++)

            @if( isset($field['inline']) && $field['inline'] )
                <label class="radio-inline" for="{{$field['name']}}_{{$optionPointer}}">
                    <input  type="radio"
                            id="{{$field['name']}}_{{$optionPointer}}"
                            name="{{$field['name']}}"
                            value="{{$value}}"
                            @include('crud::inc.field_attributes')
                            {{$optionValue == $value ? ' checked': ''}}
                    > {!! $label !!}
                </label>
            @else
                <div class="radio">
                    <label for="{{$field['name']}}_{{$optionPointer}}">
                        <input type="radio" id="{{$field['name']}}_{{$optionPointer}}" name="{{$field['name']}}" value="{{$value}}" {{$optionValue == $value ? ' checked': ''}}> {!! $label !!}
                    </label>
                </div>
            @endif
        @endforeach
    @endif

    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif

    <div class="modules" style="display: none">
        @if( isset($field['modules']) && $field['modules'] = (array)$field['modules'] )
            @foreach ($field['modules'] as $module => $label )
                @include('crud::inc.field_translatable_icon')
                <div class="checkbox">
                    <label>
                        <input type="hidden" name="{{ $module }}" value="0">
                        <input type="checkbox" value="{{ $module }}" name="permissions[]"
                            @if(in_array($module, isset($entry) ? $entry->permissions->pluck('module')->toArray() : []))
                               checked="checked"
                            @endif
                        > {!! $label !!}
                    </label>
                </div>
            @endforeach
        @endif
    </div>
</div>

@push('after_scripts')
    <script>
        $( document ).ready(function() {
            updateModulesDisplay();

            $('input:radio[name={{$field['name']}}]').change(function () {
                updateModulesDisplay();
            });

            function updateModulesDisplay() {
                $('.modules').hide();

                if ($("input[name='{{$field['name']}}']:checked").val() == {{ \Motivo\Liberiser\Base\Models\Permission::ROLE_CONTENT_MANAGER }}) {
                    $('.modules').show();
                }
            }
        });
    </script>
@endpush