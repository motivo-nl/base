@php
    $relationFields = collect($field['relationFields'])->map(function (array $item) use ($field) {
        $item['name'] = $field['relation'] . '_' . $item['name'];

        return $item;
    });
@endphp

@include('crud::inc.show_fields', ['fields' => $relationFields])

@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
@endif