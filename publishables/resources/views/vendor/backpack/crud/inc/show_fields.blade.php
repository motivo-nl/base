@foreach ($fields as $field)
    @if(! isset($field['required_permission_level']) || backpack_user()->role <= (int)$field['required_permission_level'])
        @php
            $fieldsViewNamespace = $field['view_namespace'] ?? 'crud::fields';
        @endphp

        @include($fieldsViewNamespace.'.'.$field['type'], ['field' => $field])

        @push('crud_fields_scripts')
            @if (isset($field['hide_when']))
                <script>
                    $(document).ready(function () {
                        if ($('[name={{ $field['hide_when']['field'] }}]').val() == {{ $field['hide_when']['value'] }}) {
                            $('[name={{ $field['name'] }}]').closest('.form-group').hide();
                        }

                        $('[name={{ $field['hide_when']['field'] }}]').on('change', function () {
                            let formGroup = $('[name={{ $field['name'] }}]').closest('.form-group');

                            if($(this).val() == {{ $field['hide_when']['value'] }}) {
                                formGroup.hide();
                            } else {
                                formGroup.show();
                            }
                        });
                    });
                </script>
            @endif
        @endpush
    @endif
@endforeach