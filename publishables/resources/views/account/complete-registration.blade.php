@foreach($errors->all() as $error)
    <p class="error">{{ $error }}</p>
@endforeach

<form method="POST" action="{{ request()->fullUrl() }}">
    {{ csrf_field() }}

    <label for="password">{{ ucfirst(trans('Liberiser::user.columns.password')) }}</label> <input type="password" name="password" id="password"><br>
    <label for="confirm">{{ ucfirst(trans('Liberiser::user.columns.password_confirm')) }}</label> <input type="password" name="password_confirmation" id="confirm"><br>
    <br>
    <button>{{ ucfirst(trans('Liberiser::user.form.submit')) }}</button>
</form>