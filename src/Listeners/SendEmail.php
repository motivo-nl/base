<?php

namespace Motivo\Liberiser\Base\Listeners;

use Exception;
use Illuminate\Support\Facades\Mail;
use Motivo\Liberiser\Base\Events\EmailCreated;
use Motivo\Liberiser\Base\Models\LiberiserMail;
use Motivo\Liberiser\Base\Models\LiberiserMailable;

class SendEmail
{
    public function handle(EmailCreated $event): void
    {
        $email = $event->getEmail();

        try {
            Mail::to([['email' => $email->to_address, 'name' => $email->to_name]])->send(new LiberiserMailable($email, 'Liberiser::email.plain_html', $event->getEncrypt(), $event->getData()));

            $email->status = LiberiserMail::STATUS_SENT;
            $email->save();
        } catch (Exception $e) {
            $email->status = LiberiserMail::STATUS_FAILED;
            $email->encrypt($event->getEncrypt());
            $email->save();
        }
    }
}
