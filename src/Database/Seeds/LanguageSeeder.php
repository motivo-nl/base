<?php

namespace Motivo\Liberiser\Base\Database\Seeds;

use Illuminate\Database\Seeder;
use Motivo\Liberiser\Base\Models\Language;

class LanguageSeeder extends Seeder
{
    protected $languages = [
        ['nl', 'Nederlands', 'Nederlands', 'nl_NL', 1, 1],
        ['nl_BE', 'Nederlands (Belgie)', 'Nederlands (Belgie)', 'nl_BE', 1, 1],
        ['de', 'Duits', 'Deutsch', 'de_DE', 1, 1],
        ['en', 'Engels', 'English', 'en_GB', 1, 1],
        ['es', 'Spaans', 'Español', 'es_ES', 1, 1],
        ['fr', 'Frans', 'Français', 'fr_FR', 1, 1],
        ['it', 'Italiaans', 'Italiano', 'it_IT', 0, 0],
        ['da', 'Deens', 'Dansk', 'da_DK', 0, 0],
        ['pl', 'Pools', 'Polskie', 'pl_PL', 0, 0],
        ['se', 'Zweeds', 'Svenska', 'se_SE', 0, 0],
        ['no', 'Noors', 'Norsk', 'no_NO', 0, 0],
        ['hu', 'Hongaars', 'Magyar', 'hu_HU', 0, 0],
        ['ro', 'Roemeens', 'Română', 'ro_RO', 0, 0],
        ['pt', 'Portugees', 'Português', 'pt_PT', 0, 0],
        ['il', 'Hebreeuws', 'עברית', 'he-IL', 0, 0],
        ['ca-ES', 'Catalaans', 'Catalana', 'ca-ES', 1, 1],
    ];

    public function run(): void
    {
        foreach ($this->languages as $language) {
            $mappedLanguage = $this->mapLanguage($language);

            Language::updateOrCreate(
                ['shortcode' => $mappedLanguage['shortcode']],
                $mappedLanguage
            );
        }
    }

    private function mapLanguage(array $language): array
    {
        return [
            'shortcode' => $language[0],
            'label' => $language[1],
            'label_localized' => $language[2],
            'locale' => $language[3],
            'available' => $language[4],
            'active' => $language[5],
        ];
    }
}
