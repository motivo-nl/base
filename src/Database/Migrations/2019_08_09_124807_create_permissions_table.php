<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    public function up(): void
    {
        Schema::create('liberiser_permissions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('user_id');
            $table->string('module');

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('liberiser_permissions');
    }
}
