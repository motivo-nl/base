<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiberiserEmailsTemplatesTable extends Migration
{
    public function up(): void
    {
        Schema::create('liberiser_email_templates', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('signature_id')->nullable()->default(null);

            $table->integer('type');
            $table->string('module');
            $table->string('label');
            $table->json('variables');
            $table->json('description');

            $table->json('subject')->nullable()->default(null);
            $table->json('body')->nullable()->default(null);

            $table->string('email_name');
            $table->string('email_address');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('signature_id')->references('id')->on('liberiser_email_signatures');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('liberiser_email_templates');
    }
}
