<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiberiserEmailsTable extends Migration
{
    public function up(): void
    {
        Schema::create('liberiser_emails', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('language_id');
            $table->unsignedBigInteger('template_id');

            $table->string('from_name');
            $table->string('from_address');
            $table->string('to_name');
            $table->string('to_address');

            $table->string('subject');
            $table->text('body')->nullable()->default(null);

            $table->json('variables');

            $table->integer('status');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('language_id')->references('id')->on('liberiser_languages');
            $table->foreign('template_id')->references('id')->on('liberiser_email_templates');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('liberiser_emails');
    }
}
