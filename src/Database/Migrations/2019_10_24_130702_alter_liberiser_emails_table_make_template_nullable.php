<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLiberiserEmailsTableMakeTemplateNullable extends Migration
{
    public function up(): void
    {
        Schema::table('liberiser_emails', function (Blueprint $table) {
            $table->unsignedBigInteger('template_id')->nullable()->default(null)->change();
            $table->json('variables')->nullable()->default(null)->change();
        });
    }

    public function down(): void
    {
        Schema::table('liberiser_emails', function (Blueprint $table) {
            $table->unsignedBigInteger('template_id')->nullable(false)->change();
            $table->json('variables')->nullable(false)->change();
        });
    }
}
