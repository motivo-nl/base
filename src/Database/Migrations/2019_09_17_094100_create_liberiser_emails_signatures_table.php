<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiberiserEmailsSignaturesTable extends Migration
{
    public function up(): void
    {
        Schema::create('liberiser_email_signatures', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->text('signature');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('liberiser_email_signatures');
    }
}
