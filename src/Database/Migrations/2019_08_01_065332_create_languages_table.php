<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    public function up(): void
    {
        Schema::create('liberiser_languages', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('shortcode', 16)->unique();
            $table->string('label');
            $table->string('label_localized');
            $table->string('locale', 16)->unique();
            $table->boolean('available');
            $table->boolean('active');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('liberiser_languages');
    }
}
