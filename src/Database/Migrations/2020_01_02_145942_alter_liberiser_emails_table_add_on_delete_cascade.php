<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLiberiserEmailsTableAddOnDeleteCascade extends Migration
{
    public function up(): void
    {
        Schema::table('liberiser_emails', function (Blueprint $table) {
            $table->dropForeign(['template_id']);

            $table->foreign('template_id')->references('id')->on('liberiser_email_templates')->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::table('liberiser_emails', function (Blueprint $table) {
            $table->dropForeign(['template_id']);

            $table->foreign('template_id')->references('id')->on('liberiser_email_templates');
        });
    }
}
