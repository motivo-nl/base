<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLiberiserEmailTemplatesTableAddOnDeleteCascade extends Migration
{
    public function up(): void
    {
        Schema::table('liberiser_email_templates', function (Blueprint $table) {
            $table->dropForeign(['signature_id']);

            $table->foreign('signature_id')->references('id')->on('liberiser_email_signatures')->onDelete('cascade');
        });
    }

    public function down(): void
    {
        Schema::table('liberiser_email_templates', function (Blueprint $table) {
            $table->dropForeign(['signature_id']);

            $table->foreign('signature_id')->references('id')->on('liberiser_email_signatures');
        });
    }
}
