<?php

namespace Motivo\Liberiser\Base\Http\Controllers;

use Motivo\Liberiser\Base\Models\LiberiserMail;

class LiberiserMailCrudController extends LiberiserCrudController
{
    /** @var string */
    protected $model = LiberiserMail::class;

    /** @var string */
    protected $storeRequest = null;

    /** @var string */
    protected $updateRequest = null;

    /** @var string */
    protected $crudRoute = '/emails';

    /** @var string */
    protected $entityNameSingular = 'Liberiser::emails.singular';

    /** @var string */
    protected $entityNamePlural = 'Liberiser::emails.index';

    public function setup(): void
    {
        parent::setup();

        $this->crud->denyAccess('create');
        $this->crud->denyAccess('update');
        $this->crud->denyAccess('delete');

        $this->crud->allowAccess('show');

        $this->crud->setShowView('Liberiser::email.preview');

        $this->crud->orderBy('created_at', 'desc');

        $this->crud->addFilter([
            'name' => 'status',
            'type' => 'dropdown',
            'label'=> ucfirst(trans('Liberiser::emails.status.title')),
        ], [
            LiberiserMail::STATUS_QUEUED => ucfirst(trans('Liberiser::emails.status.queued')),
            LiberiserMail::STATUS_SENT => ucfirst(trans('Liberiser::emails.status.sent')),
            LiberiserMail::STATUS_FAILED => ucfirst(trans('Liberiser::emails.status.failed')),
        ], function ($value) {
            $this->crud->addClause('where', 'status', $value);
        });
    }
}
