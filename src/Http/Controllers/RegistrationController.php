<?php

namespace Motivo\Liberiser\Base\Http\Controllers;

use App\User;
use Illuminate\View\View;
use Illuminate\Http\Response;
use Prologue\Alerts\Facades\Alert;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Motivo\Liberiser\Base\Http\Requests\Auth\AccountInfoRequest;
use Motivo\Liberiser\Base\Http\Requests\Auth\ChangePasswordRequest;
use Backpack\Base\app\Http\Controllers\Controller as BackpackController;
use Motivo\Liberiser\Base\Http\Requests\Auth\CompleteRegistrationRequest;

class RegistrationController extends BackpackController
{
    public function getCompleteRegistration(User $user): View
    {
        return view('Liberiser::account.complete-registration')->with('user', $user);
    }

    public function postCompleteRegistration(CompleteRegistrationRequest $request, User $user): RedirectResponse
    {
        $user->password = bcrypt($request->input('password'));
        $user->save();

        return redirect()->route('backpack.dashboard');
    }
}
