<?php

namespace Motivo\Liberiser\Base\Http\Controllers;

use Alert;
use Request;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Motivo\Liberiser\Base\Models\Permission;
use Motivo\Liberiser\Base\Models\EmailTemplate;
use Motivo\Liberiser\Base\Http\Requests\Emails\StoreEmailTemplateRequest;
use Motivo\Liberiser\Base\Http\Requests\Emails\UpdateEmailTemplateRequest;
use Motivo\Liberiser\Base\Http\Requests\Emails\UpdateManagerEmailTemplateRequest;

class EmailTemplateCrudController extends LiberiserCrudController
{
    /** @var string */
    protected $model = EmailTemplate::class;

    /** @var string */
    protected $storeRequest = StoreEmailTemplateRequest::class;

    /** @var string */
    protected $updateRequest = UpdateEmailTemplateRequest::class;

    /** @var string */
    protected $crudRoute = '/email-templates';

    /** @var string */
    protected $entityNameSingular = 'Liberiser::emails/email-template.singular';

    /** @var string */
    protected $entityNamePlural = 'Liberiser::emails/email-template.plural';

    public function setup(): void
    {
        parent::setup();

        $this->crud->allowAccess('create', Permission::ROLE_ADMIN);
        $this->crud->allowAccess('update', Permission::ROLE_ADMIN);
        $this->crud->allowAccess('delete', Permission::ROLE_ADMIN);
        $this->crud->allowAccess('update_manager', Permission::ROLE_CONTENT_MANAGER);

        $this->crud->addButtonFromView('line', 'update_email_template_manager', 'update_email_template_manager', 'beginning');
    }

    public function store(StoreEmailTemplateRequest $request): RedirectResponse
    {
        $store = parent::processStore($request);

        return $store;
    }

    public function update(UpdateEmailTemplateRequest $request): RedirectResponse
    {
        $update = parent::processUpdate($request);

        return $update;
    }

    public function editManager(EmailTemplate $emailTemplate): View
    {
        $this->crud->hasAccessOrFail('update_manager');
        $this->crud->setOperation('update');

        // get entry ID from Request (makes sure its the last ID for nested resources)
        $id = $emailTemplate->id;
        $entry = $this->crud->getEntry($id);

        $fields = EmailTemplate::getManagerFields($entry);

        $this->crud->update_fields = null;
        $this->crud->addFields($fields);
        $this->crud->setFormUrl('update', route('crud.email-templates.update-manager', $entry));

        // get the info for that entry
        $this->data['entry'] = $entry;
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $fields;
        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function updateManager(UpdateManagerEmailTemplateRequest $request): RedirectResponse
    {
        $this->crud->hasAccessOrFail('update_manager');
        $this->crud->setOperation('update');

        // fallback to global request instance
        if (is_null($request)) {
            $request = Request::instance();
        }

        // update the row in the db
        $item = $this->crud->update(
            $request->get($this->crud->model->getKeyName()),
            $request->except('save_action', '_token', '_method', 'current_tab', 'http_referrer')
        );
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->setSaveAction();

        return $this->performSaveAction($item->getKey());
    }
}
