<?php

namespace Motivo\Liberiser\Base\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Database\Eloquent\Model;
use Motivo\Liberiser\Base\Events\StoreBaseModelEvent;
use Motivo\Liberiser\Base\Events\UpdateBaseModelEvent;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Exceptions\MethodNotFoundException;
use Motivo\Liberiser\Base\Overrides\Backpack\CRUD\app\Http\Controllers\CrudController;

class LiberiserCrudController extends CrudController
{
    /** @var string */
    protected $model;

    /** @var string */
    protected $storeRequest;

    /** @var string */
    protected $updateRequest;

    /** @var string */
    protected $crudRoute;

    /** @var string */
    protected $entityNameSingular;

    /** @var string */
    protected $entityNamePlural;

    /** @var Model|null */
    protected $entry;

    protected $overrideableProperties = [
        'class' => 'model',
        'store_request' => 'storeRequest',
        'update_request' => 'updateRequest',
    ];

    public function __construct()
    {
        parent::__construct();

        $props = [
            'model',
            'crudRoute',
            'entityNameSingular',
            'entityNamePlural',
        ];

        foreach ($props as $prop) {
            if (! isset($this->$prop)) {
                throw new MethodNotFoundException(self::class, $prop);
            }
        }

        $this->updateOverrideableProperties();

        $this->translateEntityNames();
    }

    private function updateOverrideableProperties(): void
    {
        foreach ($this->overrideableProperties as $config => $overridable) {
            $property = $this->model::getConfig()[$config] ?? null;

            if (isset($property)) {
                $this->{$overridable} = $property;
            }
        }
    }

    private function translateEntityNames(): void
    {
        if (strpos($this->entityNameSingular, '.') > 0) {
            $this->entityNameSingular = trans($this->entityNameSingular);
        }

        if (strpos($this->entityNamePlural, '.') > 0) {
            $this->entityNamePlural = trans($this->entityNamePlural);
        }
    }

    public function setup(): void
    {
        $id = request()->route(Str::singular($this->model::getModuleName()));

        if (isset($id)) {
            $this->entry = $id;

            if (is_numeric($id)) {
                $this->entry = $this->model::find($id);
            }
        }

        $this->crud->setModel($this->model);
        $this->crud->setRoute(config('backpack.base.route_prefix').$this->crudRoute);
        $this->crud->setEntityNameStrings($this->entityNameSingular, $this->entityNamePlural);

        $this->crud->addColumns($this->model::getColumns());

        $fields = $this->model::getFields($this->entry);
        $this->crud->addFields($this->getCreateFields($fields), 'create');
        $this->crud->addFields($this->getUpdateFields($fields), 'update');

        if ($this->storeRequest) {
            $this->crud->setRequiredFields($this->storeRequest, 'create');
        }

        if ($this->updateRequest) {
            $this->crud->setRequiredFields($this->updateRequest, 'edit');
        }
    }

    public function processStore(LiberiserRequest $request): RedirectResponse
    {
        $redirectLocation = parent::storeCrud($request);

        $this->storeOrUpdateRelations($request);

        event(new StoreBaseModelEvent($this->crud->entry, $request));

        return $redirectLocation;
    }

    public function processUpdate(Request $request): RedirectResponse
    {
        $redirectLocation = parent::updateCrud($request);

        $this->storeOrUpdateRelations($request);

        event(new UpdateBaseModelEvent($this->crud->entry, $request));

        return $redirectLocation;
    }

    protected function storeOrUpdateRelations(Request $request): void
    {
        $entity = $this->crud->entry;

        $components = $this->model::getAllComponentRelations();

        foreach ($components as $component) {
            $entity->{$component}()->updateOrCreate([], $this->getRelationInput($component, $request->all()));
        }

        $entity->save();
    }

    protected function getRelationInput(string $component, array $input): array
    {
        return collect($input)->filter(function ($value, $key) use ($component) {
            return strpos($key, $component.'_') === 0;
        })->mapWithKeys(function ($item, $key) use ($component) {
            $formattedKey = substr($key, strlen($component) + 1);

            if (is_array($item)) {
                $item = implode(',', $item);
            }

            if ($item === 'null') {
                $item = null;
            }

            return [$formattedKey => $item];
        })->toArray();
    }

    private function getCreateFields(array $fields): array
    {
        return collect($fields)->filter(function ($item) {
            return
                ! is_array($item)
                || ! isset($item['for_form'])
                || in_array($item['for_form'], ['create', 'store']);
        })->toArray();
    }

    private function getUpdateFields(array $fields): array
    {
        return collect($fields)->filter(function ($item) {
            return
                ! is_array($item)
                || ! isset($item['for_form'])
                || in_array($item['for_form'], ['edit', 'update']);
        })->toArray();
    }
}
