<?php

namespace Motivo\Liberiser\Base\Http\Controllers;

use App;
use Alert;
use App\User;
use Illuminate\View\View;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\RedirectResponse;
use Motivo\Liberiser\Base\Models\Language;
use Motivo\Liberiser\Base\Models\Permission;
use Motivo\Liberiser\Base\Models\LiberiserMail;
use Motivo\Liberiser\Base\Http\Requests\Users\StoreUserRequest;
use Motivo\Liberiser\Base\Http\Requests\Users\UpdateUserRequest;
use Motivo\Liberiser\Base\Http\Controllers\LiberiserCrudController;

class UsersCrudController extends LiberiserCrudController
{
    /** @var string */
    protected $model = User::class;

    /** @var string */
    protected $storeRequest = StoreUserRequest::class;

    /** @var string */
    protected $updateRequest = UpdateUserRequest::class;

    /** @var string */
    protected $crudRoute = '/user';

    /** @var string */
    protected $entityNameSingular = 'user';

    /** @var string */
    protected $entityNamePlural = 'users';

    public function store(StoreUserRequest $request): RedirectResponse
    {
        $store = parent::processStore($request);

        $user = $this->crud->entry;

        $this->syncPermissions($user, $request);

        $this->sendMail($user);

        return $store;
    }

    public function edit($id)
    {
        $this->canEditUser();

        return parent::edit($id);
    }

    public function update(UpdateUserRequest $request): RedirectResponse
    {
        $this->canEditUser();

        $update = parent::processUpdate($request);

        $this->syncPermissions($this->crud->entry, $request);

        return $update;
    }

    public function destroy($id)
    {
        $this->canEditUser();

        return parent::destroy($id);
    }

    public function search()
    {
        if (backpack_user()->role !== Permission::ROLE_ADMIN) {
            $this->crud->addClause('where', 'role', '!=', Permission::ROLE_ADMIN);
        }

        return parent::search();
    }

    private function syncPermissions(User $user, Request $request): void
    {
        $permissions = $request->input('permissions', []);

        if ($this->crud->entry->role != Permission::ROLE_CONTENT_MANAGER) {
            $permissions = [];
        }

        Permission::syncUser($user, $permissions);
    }

    private function sendMail(User $user): void
    {
        $url = URL::signedRoute('liberiser.account.complete-registration', ['user' => $user->id]);

        $lang = Language::where('shortcode', app()->getLocale())->first();

        LiberiserMail::Send(
            $lang,
            ucfirst(trans('Liberiser::emails/registered.subject')),
            config('liberiser.config.from_name', 'Motivo'),
            config('liberiser.config.from_email', 'email@example.com'),
            $user->full_name,
            $user->email,
            'Liberiser::email.user.registered',
            ['url' => $url]
        );
    }

    private function canEditUser(): void
    {
        $entry = $this->crud->getEntry($this->crud->getCurrentEntryId());
        $user = backpack_user() ?? Auth::user();

        abort_if($entry->role === Permission::ROLE_ADMIN && $user->role !== Permission::ROLE_ADMIN, Response::HTTP_FORBIDDEN);
    }
}
