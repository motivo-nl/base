<?php

namespace Motivo\Liberiser\Base\Http\Controllers;

use App\User;
use Illuminate\View\View;
use Illuminate\Http\Response;
use Prologue\Alerts\Facades\Alert;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Backpack\Base\app\Http\Requests\ChangePasswordRequest;
use Motivo\Liberiser\Base\Http\Requests\Auth\AccountInfoRequest;
use Backpack\Base\app\Http\Controllers\Controller as BackpackController;
use Motivo\Liberiser\Base\Http\Requests\Auth\CompleteRegistrationRequest;

class AccountController extends BackpackController
{
    /** @var array */
    protected $data = [];

    public function __construct()
    {
        $this->middleware(backpack_middleware());
    }

    public function getAccountInfoForm(): View
    {
        $this->data['title'] = trans('backpack::base.my_account');
        $this->data['user'] = $this->guard()->user();

        return view('backpack::auth.account.update_info', $this->data);
    }

    public function postAccountInfoForm(AccountInfoRequest $request): RedirectResponse
    {
        $result = $this->guard()->user()->update($request->except(['_token']));

        if ($result) {
            Alert::success(trans('backpack::base.account_updated'))->flash();
        } else {
            Alert::error(trans('backpack::base.error_saving'))->flash();
        }

        return redirect()->back();
    }

    public function getChangePasswordForm(): View
    {
        $this->data['title'] = trans('backpack::base.my_account');
        $this->data['user'] = $this->guard()->user();

        return view('backpack::auth.account.change_password', $this->data);
    }

    public function postChangePasswordForm(ChangePasswordRequest $request): RedirectResponse
    {
        $user = $this->guard()->user();
        $user->password = Hash::make($request->new_password);

        if ($user->save()) {
            Alert::success(trans('backpack::base.account_updated'))->flash();
        } else {
            Alert::error(trans('backpack::base.error_saving'))->flash();
        }

        return redirect()->back();
    }

    protected function guard()
    {
        return backpack_auth();
    }
}
