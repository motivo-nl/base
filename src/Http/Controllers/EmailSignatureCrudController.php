<?php

namespace Motivo\Liberiser\Base\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Motivo\Liberiser\Base\Models\EmailSignature;
use Motivo\Liberiser\Base\Http\Requests\Emails\StoreEmailSignatureRequest;
use Motivo\Liberiser\Base\Http\Requests\Emails\UpdateEmailSignatureRequest;

class EmailSignatureCrudController extends LiberiserCrudController
{
    /** @var string */
    protected $model = EmailSignature::class;

    /** @var string */
    protected $storeRequest = StoreEmailSignatureRequest::class;

    /** @var string */
    protected $updateRequest = UpdateEmailSignatureRequest::class;

    /** @var string */
    protected $crudRoute = '/email-signatures';

    /** @var string */
    protected $entityNameSingular = 'Liberiser::emails/email-signature.singular';

    /** @var string */
    protected $entityNamePlural = 'Liberiser::emails/email-signature.plural';

    public function store(StoreEmailSignatureRequest $request): RedirectResponse
    {
        return parent::processStore($request);
    }

    public function update(UpdateEmailSignatureRequest $request): RedirectResponse
    {
        return parent::processUpdate($request);
    }
}
