<?php

namespace Motivo\Liberiser\Base\Http\Requests;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationRuleParser;
use Motivo\Liberiser\Base\Validation\Validation;
use Motivo\Liberiser\Base\Interfaces\ValidationInterface;

abstract class LiberiserRequest extends FormRequest
{
    /** @var array */
    protected $validations = [];

    protected $model;

    private $requiredProps = [
        'model',
    ];

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);

        $this->validations = Validation::getValidations(request()->route()->controller);

        foreach ($this->requiredProps as $prop) {
            if (! isset($this->$prop)) {
                throw new Exception(sprintf('Property %s not set for liberiser request %s', $prop, self::class));
            }
        }
    }

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [];

        array_map(function (ValidationInterface $validation) use (&$rules) {
            is_array($validation->rules()) ? $rules = array_merge_recursive($rules, $validation->rules()) : null;
        }, $this->validations);

        return (new ValidationRuleParser($this->all()))
            ->explode($rules)->rules;
    }

    public function transformRules(array $rules, $validationRules): array
    {
        $originalArray = $rules;

        return collect(array_merge_recursive($rules, $validationRules))
            ->map(function ($rules, $key) use ($originalArray) {
                if (
                    Arr::in($rules, ['required', 'nullable'], true) &&
                    Arr::in($originalArray[$key], ['required'], true) &&
                    ! request()->get($key)
                ) {
                    return $rules[$key] = ['nullable'];
                }

                return $rules;
            })->toArray();
    }

    protected function prepareForValidation(): void
    {
        array_map(function (ValidationInterface $validation) {
            $validation->before($this);
        }, $this->validations);
    }

    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            array_map(function (ValidationInterface $validation) use ($validator) {
                $validation->after($validator);
            }, $this->validations);
        });
    }
}
