<?php

namespace Motivo\Liberiser\Base\Http\Requests\Emails;

use Illuminate\Support\Facades\Request;
use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Contracts\Validation\Validator;
use Motivo\Liberiser\Base\Models\EmailTemplate;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\UpdateLiberiserRequest;

class UpdateEmailTemplateRequest extends UpdateLiberiserRequest
{
    protected $model = EmailTemplate::class;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        $template = Request::route('email_template');

        if (is_int($template) || is_string($template)) {
            $template = EmailTemplate::find($template);
        }

        $this->entity = $template;

        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    public function getValidatorInstance(): Validator
    {
        if (! $this->has('variables') || is_null($this->input('variables'))) {
            $this->merge(['variables' => json_encode([])]);
        }

        $validator = parent::getValidatorInstance();

        $validator->after(function (Validator $validator) {
            $id = $this->input('id');
            $module = $this->input('label');
            $label = $this->input('label');

            if (EmailTemplate::where('module', '=', $module)->where('label', '=', $label)->where('id', '!=', $id)->first()) {
                $validator->errors()->add('module_label_pair_exists', ucfirst(trans('Liberiser::email-template.error.module_label_pair_exists')));
            }
        });

        return $validator;
    }

    public static function getRules(LiberiserRequest $request, ?BaseModel $entity): array
    {
        return [
            'id' => [
                'required',
                'integer',
            ],
            'type' => [
                'required',
                'integer',
            ],
            'module' => [
                'required',
            ],
            'label' => [
                'required',
            ],
            'description' => [
                'required',
            ],
            'variables' => [
                'nullable',
                'json',
            ],
        ];
    }
}
