<?php

namespace Motivo\Liberiser\Base\Http\Requests\Emails;

use Illuminate\Support\Facades\Request;
use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Contracts\Validation\Validator;
use Motivo\Liberiser\Base\Models\EmailTemplate;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\UpdateLiberiserRequest;

class UpdateManagerEmailTemplateRequest extends UpdateLiberiserRequest
{
    protected $model = EmailTemplate::class;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        $template = Request::route('email_template');

        if (is_int($template) || is_string($template)) {
            $template = EmailTemplate::find($template);
        }

        $this->entity = $template;

        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    public function getValidatorInstance(): Validator
    {
        $this->merge(['id' => $this->entity->id]);

        return parent::getValidatorInstance();
    }

    public static function getRules(LiberiserRequest $request, ?BaseModel $entity): array
    {
        return [
            'email_name' => [
                'required',
            ],
            'email_address' => [
                'required',
                'email',
            ],
            'subject' => [
                'required',
            ],
            'body' => [
                'required',
            ],
        ];
    }
}
