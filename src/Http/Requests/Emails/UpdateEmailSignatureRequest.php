<?php

namespace Motivo\Liberiser\Base\Http\Requests\Emails;

use Illuminate\Http\Request;
use Motivo\Liberiser\Base\Models\BaseModel;
use Motivo\Liberiser\Base\Models\EmailSignature;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\UpdateLiberiserRequest;

class UpdateEmailSignatureRequest extends UpdateLiberiserRequest
{
    protected $model = EmailSignature::class;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        $signature = Request::route('email_template');

        if (is_int($signature) || is_string($signature)) {
            $signature = EmailSignature::find($signature);
        }

        $this->entity = $signature;

        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    public static function getRules(LiberiserRequest $request, ?BaseModel $entity): array
    {
        return [
            'name' => [
                'required',
            ],
            'signature' => [
                'required',
            ],
        ];
    }
}
