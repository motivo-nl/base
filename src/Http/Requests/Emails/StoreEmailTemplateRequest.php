<?php

namespace Motivo\Liberiser\Base\Http\Requests\Emails;

use Illuminate\Contracts\Validation\Validator;
use Motivo\Liberiser\Base\Models\EmailTemplate;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\StoreLiberiserRequest;

class StoreEmailTemplateRequest extends StoreLiberiserRequest
{
    protected $model = EmailTemplate::class;

    public function getValidatorInstance(): Validator
    {
        if (! $this->has('variables') || is_null($this->input('variables'))) {
            $this->merge(['variables' => json_encode([])]);
        }

        if ($this->input('type', null) === EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_SENDER) {
            $this->merge(['email_name' => config('liberiser.email_templates.default_from')]);
            $this->merge(['email_address' => config('liberiser.email_templates.default_from')]);
        } else {
            $this->merge(['email_name' => config('liberiser.email_templates.default_to')]);
            $this->merge(['email_address' => config('liberiser.email_templates.default_to')]);
        }

        $validator = parent::getValidatorInstance();

        $validator->after(function (Validator $validator) {
            $module = $this->input('module');
            $label = $this->input('label');

            if (EmailTemplate::where('module', '=', $module)->where('label', '=', $label)->first()) {
                $message = ucfirst(trans('Liberiser::emails/email-template.error.module_label_pair_exists', ['module' => $module, 'label' => $label]));

                $validator->errors()->add('module_label_pair_exists', $message);
            }
        });

        return $validator;
    }

    public static function getRules(LiberiserRequest $request): array
    {
        return [
            'type' => [
                'required',
                'integer',
            ],
            'email_name' => [
                'required',
            ],
            'email_address' => [
                'required',
                'email',
            ],
            'module' => [
                'required',
            ],
            'label' => [
                'required',
            ],
            'description' => [
                'required',
            ],
            'variables' => [
                'nullable',
                'json',
            ],
        ];
    }
}
