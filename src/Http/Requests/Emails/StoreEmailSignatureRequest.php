<?php

namespace Motivo\Liberiser\Base\Http\Requests\Emails;

use Motivo\Liberiser\Base\Models\EmailSignature;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\StoreLiberiserRequest;

class StoreEmailSignatureRequest extends StoreLiberiserRequest
{
    protected $model = EmailSignature::class;

    public static function getRules(LiberiserRequest $request): array
    {
        return [
            'name' => [
                'required',
            ],
            'signature' => [
                'required',
            ],
        ];
    }
}
