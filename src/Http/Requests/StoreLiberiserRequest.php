<?php

namespace Motivo\Liberiser\Base\Http\Requests;

abstract class StoreLiberiserRequest extends LiberiserRequest
{
    public function rules(): array
    {
        $rules = $this::getRules($this);

        foreach ($this->model::getPlugins() as $pluginName => $pluginConfig) {
            $request = config("liberiser.{$pluginName}.store_request");

            $pluginRules = $request::getRules($this);

            $rules = array_merge($rules, $pluginRules);
        }

        return $this->transformRules($rules, parent::rules());
    }

    abstract public static function getRules(LiberiserRequest $request): array;
}
