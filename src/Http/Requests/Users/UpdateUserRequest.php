<?php

namespace Motivo\Liberiser\Base\Http\Requests\Users;

use App\User;
use Illuminate\Support\Facades\Request;
use Motivo\Liberiser\Base\Models\BaseModel;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\UpdateLiberiserRequest;

class UpdateUserRequest extends UpdateLiberiserRequest
{
    protected $model = User::class;

    public function __construct(array $query = [], array $request = [], array $attributes = [], array $cookies = [], array $files = [], array $server = [], $content = null)
    {
        $user = Request::route('user');

        if (is_int($user) || is_string($user)) {
            $user = User::find($user);
        }

        $this->entity = $user;

        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    public static function getRules(LiberiserRequest $request, ?BaseModel $entity): array
    {
        $id = $entity->id ?? null;

        return [
            'first_name' => [
                'required',
            ],
            'last_name' => [
                'required',
            ],
            'email' => [
                'required',
                'unique:users,id,'.$id,
            ],
            'role' => [
                'required',
                'integer',
            ],
        ];
    }
}
