<?php

namespace Motivo\Liberiser\Base\Http\Requests\Users;

use App\User;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Http\Requests\StoreLiberiserRequest;

class StoreUserRequest extends StoreLiberiserRequest
{
    protected $model = User::class;

    public static function getRules(LiberiserRequest $request): array
    {
        return [
            'first_name' => [
                'required',
            ],
            'last_name' => [
                'required',
            ],
            'email' => [
                'required',
                'unique:users,email',
            ],
            'role' => [
                'required',
                'integer',
            ],
        ];
    }
}
