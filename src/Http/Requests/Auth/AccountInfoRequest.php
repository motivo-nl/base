<?php

namespace Motivo\Liberiser\Base\Http\Requests\Auth;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class AccountInfoRequest extends FormRequest
{
    public function authorize(): bool
    {
        return backpack_auth()->check();
    }

    protected function validationData(): array
    {
        return $this->only(backpack_authentication_column(), 'first_name', 'last_name');
    }

    public function rules(): array
    {
        $user = backpack_auth()->user();

        return [
            backpack_authentication_column() => [
                'required',
                backpack_authentication_column() == 'email' ? 'email' : '',
                Rule::unique($user->getTable())->ignore($user->getKey(), $user->getKeyName()),
            ],
            'first_name' => [
                'required',
            ],
            'last_name' => [
                'required',
            ],
        ];
    }
}
