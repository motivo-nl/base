<?php

namespace Motivo\Liberiser\Base\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class CompleteRegistrationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'password' => [
                'required',
                'confirmed',
                'min:8',
            ],
            'password_confirmation' => [
                'required',
            ],
        ];
    }
}
