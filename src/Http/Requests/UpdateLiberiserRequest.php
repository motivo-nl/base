<?php

namespace Motivo\Liberiser\Base\Http\Requests;

use Motivo\Liberiser\Base\Models\BaseModel;

abstract class UpdateLiberiserRequest extends LiberiserRequest
{
    protected $entity;

    private $requiredProps = [
        'model',
        'entity',
    ];

    public function rules(): array
    {
        $rules = $this::getRules($this, $this->entity);

        foreach ($this->model::getPlugins() as $pluginName => $pluginConfig) {
            $request = config("liberiser.{$pluginName}.update_request");

            $pluginRules = $request::getRules($this, $this->entity);

            $rules = array_merge($rules, $pluginRules);
        }

        return $this->transformRules($rules, parent::rules());
    }

    abstract public static function getRules(LiberiserRequest $request, ?BaseModel $entity): array;
}
