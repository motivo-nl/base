<?php

namespace Motivo\Liberiser\Base\Http\ViewComposers;

use Illuminate\View\View;
use Motivo\Liberiser\Base\Liberiser;
use Motivo\Liberiser\Base\BaseMenuItem;

class AdminMenuItemsComposer
{
    public function compose(View $view): void
    {
        $view->with('adminMenuItems', $this->getAdminMenuItems());
    }

    private function getAdminMenuItems(): array
    {
        return collect(Liberiser::getAdminMenuItems())->map(function (array $menu) {
            $menu['items'] = collect($menu['items'])->filter(function (BaseMenuItem $item) {
                return backpack_user()->hasPermission($item->getModule());
            });

            /** @var BaseMenuItem $item */
            foreach ($menu['items'] as $item) {
                $item->childs = collect($item->getChilds())->filter(function (BaseMenuItem $child) {
                    return backpack_user()->hasPermission($child->getModule());
                })->toArray();

                if ($item->getChilds() && count($item->getChilds()) === 1 && $item->getModule() === $item->getChilds()[0]->getModule()) {
                    $item->setIsTreeView(false);
                }
            }

            return $menu;
        })->sortBy('order')
            ->toArray();
    }
}
