<?php

namespace Motivo\Liberiser\Base\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Motivo\Liberiser\Base\Models\Permission;

class PermissionsMiddleware
{
    public function handle($request, Closure $next)
    {
        $user = backpack_user() ?? $request->user();

        if ($user && $user->role !== Permission::ROLE_ADMIN && method_exists($request->route()->controller, 'getModel')) {
            $module = $request->route()->controller->getModel()::getModuleName();

            abort_if(! $user->hasPermission($module), Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
