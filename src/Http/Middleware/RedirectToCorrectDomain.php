<?php

namespace Motivo\Liberiser\Base\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class RedirectToCorrectDomain
{
    /**
     * Redirect the user to the domain specified in the env.
     * When the environment is set to production, and the incoming domain does not match the domain name specified
     * in the env file, the user will be redirected to the a (secure) domain specified in the env.
     *
     * @param Request $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (App::environment() === 'production' && strpos($request->url(), env('APP_URL')) === false) {
            return redirect()->secure($this->getRedirectTo($request));
        }

        return $next($request);
    }

    private function getRedirectTo(Request $request): string
    {
        $urlInfo = parse_url($request->getUri());

        $url = $this->getDomain().$urlInfo['path'];

        if (isset($urlInfo['query'])) {
            $url = $url.'?'.$urlInfo['query'];
        }

        return $url;
    }

    private function getDomain(): string
    {
        $domain = env('APP_URL');

        if (substr($domain, strlen($domain) - 1) === '/') {
            $domain = substr($domain, 0, strlen($domain) - 1);
        }

        return $domain;
    }
}
