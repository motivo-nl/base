<?php

namespace Motivo\Liberiser\Base\Exceptions;

use Exception;
use Throwable;

class ConfigNotFoundException extends Exception
{
    public function __construct(string $component, $code = 0, Throwable $previous = null)
    {
        parent::__construct("Config for component 'liberiser/{$component}' not found.", $code, $previous);
    }
}
