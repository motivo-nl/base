<?php

namespace Motivo\Liberiser\Base\Exceptions;

use Exception;
use Throwable;

class ClassNotFoundException extends Exception
{
    public function __construct(string $class, $code = 0, Throwable $previous = null)
    {
        parent::__construct("Class {$class} does not exist.", $code, $previous);
    }
}
