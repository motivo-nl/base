<?php

namespace Motivo\Liberiser\Base\Exceptions;

use Exception;
use Throwable;

class FileNotFoundException extends Exception
{
    public static function handle(string $file, $code = 0, Throwable $previous = null): self
    {
        return new self(
            sprintf('Could not open file %s', $file),
            $code,
            $previous
        );
    }
}
