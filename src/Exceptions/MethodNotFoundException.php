<?php

namespace Motivo\Liberiser\Base\Exceptions;

use Exception;
use Throwable;

class MethodNotFoundException extends Exception
{
    public function __construct(string $class, string $property, $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('Property %s not set for crud controller %s', $property, $class), $code, $previous);
    }
}
