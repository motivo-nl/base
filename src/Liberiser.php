<?php

namespace Motivo\Liberiser\Base;

use Illuminate\Support\Facades\Facade;

/**
 * @method static BaseMenuItem[]|array getAdminMenuItems()
 * @method static self setAdminMenuItems(BaseMenuItem ...$menuItems)
 * @method static array getPageModules()
 * @method static self addPageModule(string $moduleName)
 * @method static string getEnvironmentFilePath(string $file)
 * @method static bool setEnvironmentValue(string $environmentFile, array $values)
 *
 * @see \Motivo\Liberiser\Base\LiberiserManager
 */
class Liberiser extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'liberiser';
    }
}
