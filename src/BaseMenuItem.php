<?php

namespace Motivo\Liberiser\Base;

use Throwable;
use JsonSerializable;

class BaseMenuItem implements JsonSerializable
{
    public const CATEGORY_MODULES = 'modules';

    public const CATEGORY_MANAGEMENT = 'management';

    public const CATEGORY_TOOLS = 'tools';

    /** @var string|null */
    public $icon = null;

    /** @var string|null */
    public $label = null;

    /** @var string|null */
    public $link = null;

    /** @var string|null */
    public $module = null;

    /** @var bool */
    public $isTreeView = null;

    /** @var BaseMenuItem[]|array */
    public $childs = [];

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @throws Throwable
     */
    public function getLabel(): string
    {
        throw_if(
            ! $this->label,
            sprintf('Label for menu item "%s" is not defined', self::class)
        );

        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @throws Throwable
     */
    public function getLink(): ?string
    {
        if ($this->isTreeView) {
            return null;
        }

        throw_if(
            ! $this->isTreeView && ! $this->link,
            'No link is set for the menu item.'
        );

        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function isTreeView(): bool
    {
        if (isset($this->isTreeView)) {
            return $this->isTreeView;
        }

        return count($this->childs) > 0;
    }

    public function setIsTreeView(bool $isTreeView): self
    {
        $this->isTreeView = $isTreeView;

        return $this;
    }

    public function getChilds(): ?array
    {
        if (! $this->isTreeView()) {
            return null;
        }

        return $this->childs;
    }

    public function setChilds(self ...$childs): self
    {
        $this->childs = $childs;

        return $this;
    }

    public function setModule(string $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getModule(): ?string
    {
        return $this->module;
    }

    public function jsonSerialize(): array
    {
        return [
            'icon' => $this->getIcon(),
            'label' => $this->getLabel(),
            'link' => $this->getLink(),
            'isTreeView' => $this->isTreeView(),
            'childs' => $this->getChilds(),
        ];
    }
}
