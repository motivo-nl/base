<?php

namespace Motivo\Liberiser\Base\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class PublishBackpackConfigCommand extends GeneratorCommand
{
    /** @var string */
    protected $signature = 'liberiser:publish-backpack-config';

    /** @var string */
    protected $description = 'Publish the updated backpack config.';

    public function handle(): void
    {
        $destinationPath = config_path('backpack/base.php');

        $this->info('Publishing backpack config file...');

        if ($this->files->exists($destinationPath)) {
            if ($this->confirm('backpack/base.php already exists, override existing file?.')) {
                $this->publishFiles($destinationPath);
            } else {
                $this->warn('config backpack/base.php not published.');
            }
        } else {
            $this->publishFiles($destinationPath);
        }
    }

    protected function publishFiles(string $destinationPath): void
    {
        $this->makeDirectory($destinationPath);

        $this->files->put($destinationPath, $this->buildClass());

        $this->info('Config backpack/base.php created successfully.');
    }

    protected function getStub(): string
    {
        return __DIR__.'/../../../config/backpack/backpack-base.php';
    }

    protected function buildClass($name = false): string
    {
        return $this->files->get($this->getStub());
    }
}
