<?php

namespace Motivo\Liberiser\Base\Console\Commands;

use Illuminate\Console\Command;

class LiberiserSeederCommand extends Command
{
    protected $signature = 'liberiser:seed {--force}';

    /** @var string */
    protected $description = 'Finish the base liberiser installation.';

    public function handle(): void
    {
        $this->seedBase();
        $this->seedModules();
    }

    private function seedBase(): void
    {
        $seeders = config('liberiser.config.seeders', []);

        $this->seed($seeders);
    }

    private function seedModules(): void
    {
        $modules = config('liberiser.config.modules', []);

        foreach ($modules as $module => $settings) {
            $this->seedModule($module);
        }
    }

    private function seedModule(string $module): void
    {
        $seeders = config("liberiser.{$module}.seeders", []);

        $this->seed($seeders);
    }

    private function seed(array $seeders): void
    {
        foreach ($seeders as $seeder) {
            if (! class_exists($seeder)) {
                $this->warn("Seeder '{$seeder}' not found, skipped.");

                continue;
            }

            $args = ['--class' => $seeder];
            if ($this->option('force')) {
                $args['--force'] = true;
            }
            $this->call('db:seed', $args);

            $this->info("{$seeder} succesfully seeded.");
        }
    }
}
