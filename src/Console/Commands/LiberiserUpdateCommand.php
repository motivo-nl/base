<?php

namespace Motivo\Liberiser\Base\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class LiberiserUpdateCommand extends Command
{
    /** @var string */
    private $rootPath;

    /** @var string */
    protected $signature = 'liberiser:update {--force : Overwrite any existing files}';

    /** @var string */
    protected $description = 'Finish the base liberiser installation.';

    /** @var bool */
    protected $isForced;

    public function __construct()
    {
        parent::__construct();

        $this->rootPath = app()->environmentPath().DIRECTORY_SEPARATOR;
    }

    public function handle(): void
    {
        $this->isForced = $this->option('force');

        exec('composer update');

        $this->warn('Updating backpack files...');
        Artisan::call($this->forcedCommand('vendor:publish --tag=liberiser:backpack'));

        $this->warn('Updating liberiser config files...');
        Artisan::call($this->forcedCommand('vendor:publish --tag=liberiser:config'));
    }

    public function forcedCommand(string $command): string
    {
        $suffix = $this->isForced ? ' --force' : '';

        return $command.$suffix;
    }
}
