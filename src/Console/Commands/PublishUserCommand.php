<?php

namespace Motivo\Liberiser\Base\Console\Commands;

use ErrorException;
use Illuminate\Console\Command;
use Motivo\Liberiser\Base\Liberiser;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Console\GeneratorCommand;
use Backpack\Base\app\Console\Commands\PublishBackpackUserModel;

class PublishUserCommand extends GeneratorCommand
{
    /** @var string */
    protected $signature = 'liberiser:publish-user';

    /** @var string */
    protected $description = 'Publish the updated user model.';

    public function handle(): void
    {
        $destinationPath = app_path('User.php');

        $this->info('Publishing user file...');

        if ($this->files->exists($destinationPath)) {
            if ($this->confirm($this->laravel->getNamespace().'User.php already exists, override existing file?.')) {
                $this->publishFiles($destinationPath);
            } else {
                $this->warn($this->laravel->getNamespace().'User.php not published.');
            }
        } else {
            $this->publishFiles($destinationPath);
        }
    }

    protected function publishFiles(string $destinationPath): void
    {
        $this->makeDirectory($destinationPath);

        $this->files->put($destinationPath, $this->buildClass());

        $this->info($this->laravel->getNamespace().'User.php created successfully.');
    }

    protected function getStub(): string
    {
        return __DIR__.'/../../../app/User.php';
    }

    protected function buildClass($name = false): string
    {
        $stub = $this->files->get($this->getStub());

        return $this->makeReplacements($stub);
    }

    protected function makeReplacements(&$stub): string
    {
        $stub = str_replace('Backpack\Base\app\Models;', $this->laravel->getNamespace().'Models;', $stub);

        if (! $this->files->exists(app_path('User.php')) && $this->files->exists(app_path('Models/User.php'))) {
            $stub = str_replace($this->laravel->getNamespace().'User', $this->laravel->getNamespace().'Models\User', $stub);
        }

        return $stub;
    }
}
