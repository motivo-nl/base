<?php

namespace Motivo\Liberiser\Base\Console\Commands;

use App\User;
use ErrorException;
use Illuminate\Console\Command;
use Motivo\Liberiser\Base\Liberiser;
use Illuminate\Support\Facades\Artisan;
use Motivo\Liberiser\Base\Models\Permission;

class LiberiserCreateUserCommand extends Command
{
    /** @var string */
    protected $signature = 'liberiser:create-user';

    /** @var string */
    protected $description = 'Create a liberiser user.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $firstName = null;
        $lastName = null;
        $email = null;
        $password = null;
        $role = null;

        while ($firstName === null) {
            $firstName = $this->ask('What is your first name?', null);

            if ($firstName === null) {
                $this->error('Name is required.');
            }
        }

        while ($lastName === null) {
            $lastName = $this->ask('What is your last name?', null);

            if ($lastName === null) {
                $this->error('Name is required.');
            }
        }

        while ($email === null) {
            $email = $this->ask('What is your email?', null);

            if ($email === null) {
                $this->error('Email is required.');
            } elseif (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email = null;
                $this->error('Email must be a valid email address.');
            }
        }

        while ($password === null) {
            $password = $this->secret('What is your password?', null);

            if ($password === null) {
                $this->error('Password is required.');
            }

            if (strlen($password) < 6) {
                $password = null;

                $this->error('Password must be at least 6 characters long.');
            }
        }

        $availableRoles = [
            Permission::ROLE_ADMIN => 'Super Admin',
            Permission::ROLE_MANAGER => 'Admin',
            Permission::ROLE_CONTENT_MANAGER => 'Content Manager',
        ];

        $this->info('Available user roles. Use the index to set a role.');
        foreach ($availableRoles as $roleId => $message) {
            $this->info("[{$roleId}] - {$message}");
        }

        while ($role === null) {
            $role = $this->ask('What is the users role?', null);

            if ($role === null || ! is_numeric($role) || ! isset($availableRoles[$role])) {
                $role = null;

                $this->error('Role is required. Please use one of the available roles. Make sure to use the index number.');
            }
        }

        User::create(['first_name' => $firstName, 'last_name' => $lastName, 'email' => $email, 'password' => bcrypt($password), 'role' => $role]);

        $this->info('User has been saved');
    }
}
