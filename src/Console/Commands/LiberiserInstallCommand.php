<?php

namespace Motivo\Liberiser\Base\Console\Commands;

use ErrorException;
use Illuminate\Console\Command;
use Motivo\Liberiser\Base\Liberiser;
use Illuminate\Support\Facades\Artisan;

class LiberiserInstallCommand extends Command
{
    /** @var string */
    private $rootPath;

    /** @var string */
    protected $signature = 'liberiser:install {--force : Overwrite any existing files}';

    /** @var string */
    protected $description = 'Finish the base liberiser installation.';

    public function __construct()
    {
        parent::__construct();

        $this->rootPath = app()->environmentPath().DIRECTORY_SEPARATOR;
    }

    public function handle(): void
    {
        $isForced = $this->option('force');

        $this->info('Starting installation');

        $this->publishTags($isForced);

        $this->setupSentry();

        $this->runMigrationAndSeeders();

        $this->call('liberiser:publish-backpack-config');

        $this->call('liberiser:publish-user');

        $this->info('Installation finished');
    }

    private function setupSentry(): ?string
    {
        $this->info('Setting up your sentry key');

        $sentryKey = $this->ask('What\'s your sentry key');

        if ($sentryKey) {
            $this->setSentryKeyForEnvironment($sentryKey);
        }

        return $sentryKey;
    }

    private function runMigrationAndSeeders(): void
    {
        $this->info('Running Migrations...');

        $this->call('migrate');

        $this->info('Finished migration, start seeding...');

        $this->call('liberiser:seed');

        $this->info('Finished seeding');
    }

    private function publishTags(bool $isForced): void
    {
        if (file_exists(config_path('liberiser/config.php')) && ! $isForced) {
            return;
        }

        $logFunction = $isForced ? 'warn' : 'line';
        $logText = $isForced ? 'Note: force publishing liberiser tags' : 'Publishing liberiser tags';

        $this->{$logFunction}($logText);

        $this->line('Publishing config file');

        Artisan::call('vendor:publish', [
            '--tag' => 'liberiser:config',
            '--force' => $isForced,
        ]);

        Artisan::call('vendor:publish', [
            '--tag' => 'liberiser:config-main',
            '--force' => $isForced,
        ]);

        $this->line('Publishing exception handler');

        Artisan::call('vendor:publish', [
            '--tag' => 'liberiser:exception-handler',
            '--force' => true,
        ]);

        $this->line('Publishing backpack navigation file');

        Artisan::call('vendor:publish', [
            '--tag' => 'liberiser:backpack_navigation',
            '--force' => $isForced,
        ]);
    }

    public function setSentryKeyForEnvironment(string $sentryKey): void
    {
        $environmentFile = '.env.'.$this->choice('For which environment should the key be set', ['testing', 'production']);

        if (! file_exists(Liberiser::getEnvironmentFilePath($environmentFile))) {
            try {
                copy($this->rootPath.'.env', $this->rootPath.$environmentFile);
            } catch (ErrorException $exception) {
                $this->error(sprintf('Could not create %s file.', $environmentFile));
            }

            $this->line(sprintf('Created %s based on .env file', $environmentFile));
        }

        Liberiser::setEnvironmentValue(
            $environmentFile,
            [
                'SENTRY_LARAVEL_DSN' => "\"$sentryKey\"",
            ]
        );

        $this->line(sprintf('Sentry key added for %s file', $environmentFile));
    }
}
