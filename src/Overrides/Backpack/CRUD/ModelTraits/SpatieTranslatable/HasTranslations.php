<?php

namespace Motivo\Liberiser\Base\Overrides\Backpack\CRUD\ModelTraits\SpatieTranslatable;

use Motivo\Liberiser\Base\Models\Language;
use Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations as OriginalHasTranslations;

trait HasTranslations
{
    use OriginalHasTranslations;

    /**
     * Use the forced locale if present.
     *
     * @param string $key
     * @return mixed
     */
    public function getAttributeValue($key)
    {
        if (! $this->isTranslatableAttribute($key)) {
            return parent::getAttributeValue($key);
        }

        $translation = $this->getTranslation($key, $this->getLocale() ?: config('app.locale'));

        // if it's a fake field, json_encode it
        if (is_array($translation)) {
            return json_encode($translation, JSON_UNESCAPED_UNICODE);
        }

        return $translation;
    }

    /**
     * Create translated items as json.
     *
     * @param array $attributes
     * @return static
     */
    public static function create(array $attributes = [])
    {
        $locale = $attributes['locale'] ?? \App::getLocale();
        $attributes = array_except($attributes, ['locale']);
        $non_translatable = [];

        $model = new static();

        // do the actual saving
        foreach ($attributes as $attribute => $value) {
            if ($model->isTranslatableAttribute($attribute) && in_array($attribute, $model->fillable)) { // the attribute is translatable
                $model->setTranslation($attribute, $locale, $value);
            } else { // the attribute is NOT translatable
                $non_translatable[$attribute] = $value;
            }
        }
        $model->fill($non_translatable)->save();

        return $model;
    }

    /**
     * Update translated items as json.
     *
     * @param array $attributes
     * @param array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        if (! $this->exists) {
            return false;
        }

        $locale = $attributes['locale'] ?? \App::getLocale();
        $attributes = array_except($attributes, ['locale']);
        $non_translatable = [];

        // do the actual saving
        foreach ($attributes as $attribute => $value) {
            if ($this->isTranslatableAttribute($attribute) && in_array($attribute, $this->fillable)) { // the attribute is translatable
                $this->setTranslation($attribute, $locale, $value);
            } else { // the attribute is NOT translatable
                $non_translatable[$attribute] = $value;
            }
        }

        return $this->fill($non_translatable)->save($options);
    }

    /**
     * Get all locales the admin is allowed to use.
     *
     * @return array
     */
    public function getAvailableLocales(): array
    {
        return Language::active()->mapWithKeys(function (Language $lang) {
            return [$lang->shortcode => $lang->label];
        })->toArray();
    }
}
