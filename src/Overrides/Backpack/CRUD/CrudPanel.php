<?php

namespace Motivo\Liberiser\Base\Overrides\Backpack\CRUD;

use Motivo\Liberiser\Base\Models\Permission;
use Backpack\CRUD\CrudPanel as BackpackCrudPanel;
use Backpack\CRUD\Exception\AccessDeniedException;
use Motivo\Liberiser\Base\Overrides\Backpack\CRUD\PanelTraits\BackpackUpdate as Update;

class CrudPanel extends BackpackCrudPanel
{
    use Update;

    /** @var array */
    protected $formUrl = [];

    /** @var array */
    protected $accessRolePermissions = [];

    public function allowAccess($access, ?int $role = null): array
    {
        if ($role) {
            $this->accessRolePermissions[$access] = $role;
        }

        return $this->access = array_merge(array_diff((array) $access, $this->access), $this->access);
    }

    public function hasAccess($permission): bool
    {
        if (! in_array($permission, $this->access)) {
            return false;
        }

        $user = backpack_user() ?? request()->user();
        $role = $user ? $user->role : Permission::ROLE_CONTENT_MANAGER;

        if (isset($this->accessRolePermissions[$permission]) && $role > $this->accessRolePermissions[$permission]) {
            return false;
        }

        return true;
    }

    public function hasAccessToAny($permission_array): bool
    {
        foreach ($permission_array as $key => $permission) {
            if ($this->hasAccess($permission)) {
                return true;
            }
        }

        return false;
    }

    public function hasAccessToAll($permission_array): bool
    {
        foreach ($permission_array as $key => $permission) {
            if (! $this->hasAccess($permission)) {
                return false;
            }
        }

        return true;
    }

    public function hasAccessOrFail($permission): bool
    {
        if (! $this->hasAccess($permission)) {
            throw new AccessDeniedException(trans('backpack::crud.unauthorized_access', ['access' => $permission]));
        }

        return true;
    }

    public function setFormUrl(string $type, string $url): void
    {
        $this->formUrl[$type] = $url;
    }

    public function getFormUrl(string $type, string $suffix = ''): string
    {
        if (isset($this->formUrl[$type])) {
            return $this->formUrl[$type];
        }

        if ($type === 'update') {
            return url($this->route.'/'.$this->entry->getKey()).$suffix;
        }

        return url($this->route).$suffix;
    }

    /**
     * {@inheritdoc}
     */
    public function checkIfFieldIsFirstOfItsType($field): bool
    {
        $fields_array = $this->getCurrentFields();
        $first_field = $this->getFirstFieldOfItsType($field['type'], $fields_array);

        if (! $first_field) {
            return false;
        }

        $fieldName = isset($first_field['relation'])
            ? "{$first_field['relation']}_{$first_field['name']}"
            : $first_field['name'];

        if ($field['name'] == $fieldName) {
            return true;
        }

        return false;
    }

    public function getFirstFieldOfItsType(string $type, array $array): ?array
    {
        $mappedArray = collect($array)->reduce(function (array $carry, array $item) {
            if ($item['type'] === 'liberiser_relation') {
                foreach ($item['relationFields'] as $field) {
                    $field['relation'] = $item['relation'];
                    $carry["{$item['relation']}_{$field['name']}"] = $field;
                }
            } else {
                $carry[$item['name']] = $item;
            }

            return $carry;
        }, []);

        return array_first($mappedArray, function ($item) use ($type) {
            return $item['type'] == $type;
        });
    }
}
