<?php

namespace Motivo\Liberiser\Base\Overrides\Backpack\CRUD\app\Http\Controllers;

use Motivo\Liberiser\Base\Overrides\Backpack\CRUD\CrudPanel;
use Backpack\CRUD\app\Http\Controllers\CrudController as BackpackCrudController;
use Motivo\Liberiser\Base\Overrides\Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Motivo\Liberiser\Base\Overrides\Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

class CrudController extends BackpackCrudController
{
    use CreateOperation;
    use UpdateOperation;

    public function __construct()
    {
        if (! $this->crud) {
            $this->crud = app()->make(CrudPanel::class);

            $this->middleware(function ($request, $next) {
                $this->request = $request;
                $this->crud->request = $request;
                $this->setup();

                return $next($request);
            });
        }
    }

    public function getModel(): string
    {
        return $this->model;
    }
}
