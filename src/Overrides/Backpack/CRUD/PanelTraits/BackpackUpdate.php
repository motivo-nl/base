<?php

namespace Motivo\Liberiser\Base\Overrides\Backpack\CRUD\PanelTraits;

use Backpack\CRUD\PanelTraits\Update;

trait BackpackUpdate
{
    use Update;

    public function getUpdateFields($id)
    {
        $fields = $this->update_fields;
        $entry = $this->getEntry($id);

        foreach ($fields as &$field) {
            // set the value
            if (! isset($field['value'])) {
                if (isset($field['subfields'])) {
                    $field['value'] = [];
                    foreach ($field['subfields'] as $subfield) {
                        $field['value'][] = $entry->{$subfield['name']};
                    }
                } elseif (isset($field['relationFields'])) {
                    foreach ($field['relationFields'] as $subfieldIndex => $subfield) {
                        $field['relationFields'][$subfieldIndex]['value'] = $this->getModelAttributeValue($entry, $subfield);
                    }
                } else {
                    $field['value'] = $this->getModelAttributeValue($entry, $field);
                }
            }
        }

        if (! array_key_exists('id', $fields)) {
            $fields['id'] = [
                'name'  => $entry->getKeyName(),
                'value' => $entry->getKey(),
                'type'  => 'hidden',
            ];
        }

        return $fields;
    }
}
