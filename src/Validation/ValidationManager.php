<?php

namespace Motivo\Liberiser\Base\Validation;

use Motivo\Liberiser\Base\Collections\ValidationCollection;
use Motivo\Liberiser\Base\LiberiserValidationResolverServiceProvider;

class ValidationManager
{
    public function getCollection(): ValidationCollection
    {
        return LiberiserValidationResolverServiceProvider::$validationCollection;
    }

    public function getValidations(object $for): array
    {
        $collections = self::getCollection()->get(get_class($for));

        if (! $collections) {
            return [];
        }

        return $collections;
    }

    public function addValidation(string $for, array $validation): void
    {
        $currentValidations = self::getCollection()->get($for);

        if ($currentValidations) {
            $validation = array_merge($validation, $currentValidations);
        }

        self::getCollection()->put($for, $validation);
    }
}
