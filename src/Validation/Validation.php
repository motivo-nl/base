<?php

namespace Motivo\Liberiser\Base\Validation;

use Illuminate\Support\Facades\Facade;
use Motivo\Liberiser\Base\Collections\ValidationCollection;

/**
 * @method static ValidationCollection getCollection()
 * @method static array getValidations(object $for)
 * @method static void addValidation(string $for, array $validation)
 *
 * @see \Motivo\Liberiser\Base\Validation\ValidationManager
 */
class Validation extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'validation';
    }
}
