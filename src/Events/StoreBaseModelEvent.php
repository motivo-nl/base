<?php

namespace Motivo\Liberiser\Base\Events;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Queue\SerializesModels;
use Motivo\Liberiser\Base\Models\BaseModel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class StoreBaseModelEvent extends Event
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var BaseModel */
    public $model;

    /** @var Request */
    public $request;

    /** @var array */
    public $data;

    public function __construct(BaseModel $model, Request $request, ...$data)
    {
        $this->model = $model;
        $this->request = $request;
        $this->data = $data;
    }
}
