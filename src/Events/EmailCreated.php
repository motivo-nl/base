<?php

namespace Motivo\Liberiser\Base\Events;

use Motivo\Liberiser\Base\Models\LiberiserMail;

class EmailCreated
{
    /** @var LiberiserMail */
    private $email;

    /** @var array */
    private $encrypt;

    /** @var array */
    private $data;

    public function __construct(LiberiserMail $email, array $encrypt = [], array $data = [])
    {
        $this->email = $email;
        $this->encrypt = $encrypt;
        $this->data = $data;
    }

    public function getEmail(): LiberiserMail
    {
        return $this->email;
    }

    public function getEncrypt(): array
    {
        return $this->encrypt;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
