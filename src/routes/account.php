<?php

Route::group([
    'prefix'     => config('backpack.base.route_prefix'),
    'middleware' => 'web',
    'namespace'  => 'Motivo\Liberiser\Base\Http\Controllers',
], function () {
    if (config('backpack.base.setup_my_account_routes')) {
        Route::get('edit-account-info', 'AccountController@getAccountInfoForm')->name('backpack.account.info');
        Route::post('edit-account-info', 'AccountController@postAccountInfoForm');
        Route::get('change-password', 'AccountController@getChangePasswordForm')->name('backpack.account.password');
        Route::post('change-password', 'AccountController@postChangePasswordForm');
    }
});

Route::group([
    'prefix'     => 'account',
    'middleware' => 'web',
    'namespace'  => 'Motivo\Liberiser\Base\Http\Controllers',
], function () {
    Route::get('/{user}/complete-registration', 'RegistrationController@getCompleteRegistration')->middleware(['signed', 'guest'])->name('liberiser.account.complete-registration');
    Route::post('/{user}/complete-registration', 'RegistrationController@postCompleteRegistration')->middleware(['signed', 'guest'])->name('liberiser.account.complete-registration');
});
