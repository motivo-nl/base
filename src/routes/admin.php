<?php

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'Motivo\Liberiser\Base\Http\Controllers',
], function () {
    CRUD::resource('user', 'UsersCrudController');

    Route::group(['prefix' => 'email-templates'], function () {
        Route::get('/{email_template}/edit-manager', 'EmailTemplateCrudController@editManager')->name('crud.email-templates.update-manager');
        Route::put('/{email_template}/edit-manager', 'EmailTemplateCrudController@updateManager')->name('crud.email-templates.update-manager');
    });
    CRUD::resource('email-templates', 'EmailTemplateCrudController');

    CRUD::resource('email-signatures', 'EmailSignatureCrudController');

    CRUD::resource('emails', 'LiberiserMailCrudController');
});
