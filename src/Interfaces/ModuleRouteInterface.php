<?php

namespace Motivo\Liberiser\Base\Interfaces;

interface ModuleRouteInterface
{
    public static function registerRoutes(string $url): void;
}
