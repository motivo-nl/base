<?php

namespace Motivo\Liberiser\Base\Interfaces;

use Illuminate\Validation\Validator;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;

interface ValidationInterface
{
    public function before(LiberiserRequest $request): void;

    public function rules(): ?array;

    public function after(Validator $validator): void;
}
