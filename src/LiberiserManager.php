<?php

namespace Motivo\Liberiser\Base;

use Illuminate\Support\Traits\Macroable;
use Motivo\Liberiser\Base\Exceptions\FileNotFoundException;

class LiberiserManager
{
    use Macroable;

    public $menuItems = [];

    public $pageModules = [];

    /**
     * @return BaseMenuItem[]|array
     */
    public function getAdminMenuItems(): array
    {
        return $this->menuItems;
    }

    public function setAdminMenuItems(string $category, ?string $title, BaseMenuItem ...$menuItems): self
    {
        if (! isset($this->menuItems[$category])) {
            $this->menuItems[$category] = [
                'title' => 'placeholder',
                'items' => [],
                'order' => count($this->menuItems),
            ];
        }

        if (isset($title)) {
            $this->menuItems[$category]['title'] = $title;
        }

        $this->menuItems[$category]['items'] = collect(array_merge($this->menuItems[$category]['items'], $menuItems))->unique()->toArray();

        return $this;
    }

    public function setAdminMenuItemOrder(string $category, int $order): bool
    {
        if (isset($this->menuItems[$category])) {
            $item = $this->menuItems[$category];

            foreach ($this->menuItems as &$menuItem) {
                if ($menuItem['order'] >= $order) {
                    $menuItem['order'] += 1;
                }
            }

            $this->menuItems[$category]['order'] = $order;
        }

        return false;
    }

    public function getPageModules(): array
    {
        return array_unique($this->pageModules);
    }

    public function getActiveModules(): array
    {
        $modules = array_keys(config('liberiser.config.modules', []));

        return collect($modules)->filter(function (string $item) {
            $class = config("liberiser.{$item}.class", null);

            if (! $class) {
                return false;
            }

            $config = $class::getConfig();

            return ! (isset($config['active']) && $config['active'] === false);
        })->toArray();
    }

    public function addPageModule(string $moduleName, string $label): self
    {
        $this->pageModules[$moduleName] = $label;

        return $this;
    }

    public function getEnvironmentFilePath(string $file): string
    {
        return app()->environmentPath().DIRECTORY_SEPARATOR.$file;
    }

    public function setEnvironmentValue(string $environmentFile, array $values): bool
    {
        $envFilePath = $this->getEnvironmentFilePath($environmentFile);

        if (! file_exists($envFilePath)) {
            FileNotFoundException::handle($envFilePath);
        }

        $content = file_get_contents($envFilePath);

        if (count($values) > 0) {
            foreach ($values as $envKey => $envValue) {
                $content .= "\n";

                $keyPosition = strpos($content, "{$envKey}=");

                $endOfLinePosition = strpos($content, "\n", $keyPosition);

                $oldLine = substr($content, $keyPosition, $endOfLinePosition - $keyPosition);

                if (! $keyPosition || ! $endOfLinePosition || ! $oldLine) {
                    $content .= "{$envKey}={$envValue}\n";
                } else {
                    $content = str_replace($oldLine, "{$envKey}={$envValue}", $content);
                }
            }
        }

        $content = substr($content, 0, -1);

        if (! file_put_contents($envFilePath, $content)) {
            return false;
        }

        return true;
    }
}
