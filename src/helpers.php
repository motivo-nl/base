<?php

use App\User;
use Backpack\Base\app\Models\BackpackUser;
use Motivo\Liberiser\Base\Models\Permission;
use Illuminate\Contracts\Auth\Authenticatable;

if (! function_exists('get_module_config')) {
    function get_module_config(string $module): ?array
    {
        $config = config("liberiser.{$module}", null);

        if (! isset($config)) {
            return null;
        }

        return $config['class']::getConfig();
    }
}

if (! function_exists('role_name')) {
    /**
     * @param BackpackUser|User|Authenticatable $user
     */
    function role_name($user): ?string
    {
        if (! $user) {
            return null;
        }

        switch ($user->role) {
            case Permission::ROLE_ADMIN:
                return ucfirst(trans('Liberiser::user.role.admin'));
            case Permission::ROLE_MANAGER:
                return ucfirst(trans('Liberiser::user.role.manager'));
            case Permission::ROLE_CONTENT_MANAGER:
                return ucfirst(trans('Liberiser::user.role.content_manager'));
            default:
                return null;
        }
    }
}
