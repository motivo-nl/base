<?php

namespace Motivo\Liberiser\Base\Models;

use App\User;
use mysql_xdevapi\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Permission extends Model
{
    public const ROLE_ADMIN = 1;

    public const ROLE_MANAGER = 2;

    public const ROLE_CONTENT_MANAGER = 3;

    protected $table = 'liberiser_permissions';

    protected $fillable = ['user_id', 'module'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public static function syncUser(User $user, array $permissions): void
    {
        $old = $user->permissions->pluck('module')->toArray();

        $new = array_diff($permissions, $old);
        $deleted = array_diff($old, $permissions);

        $user->permissions()->whereIn('module', $deleted)->delete();

        foreach ($new as $permission) {
            $user->permissions()->create(['module' => $permission]);
        }
    }
}
