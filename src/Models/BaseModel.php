<?php

namespace Motivo\Liberiser\Base\Models;

use Closure;
use Exception;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Traits\Macroable;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Motivo\Liberiser\Base\Http\Requests\LiberiserRequest;
use Motivo\Liberiser\Base\Exceptions\ClassNotFoundException;
use Motivo\Liberiser\Base\Exceptions\ConfigNotFoundException;
use Motivo\Liberiser\Base\Overrides\Backpack\CRUD\ModelTraits\SpatieTranslatable\HasTranslations;

abstract class BaseModel extends EloquentModel
{
    use Macroable, CrudTrait, HasTranslations;

    protected $translatable = [];

    abstract public static function getModuleName(): string;

    abstract public static function getComponentFields(?self $model): array;

    abstract public static function getComponentColumns(): array;

    abstract public static function getComponentRelations(): array;

    abstract public function getDisplayNameAttribute(): string;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->translatable = static::getTranslatableFields();
    }

    public static function boot(): void
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->with(static::relations());
        });
    }

    public static function getModules(): array
    {
        static $modules = null;

        if ($modules) {
            return $modules;
        }

        $moduleName = static::getModuleName();
        $modules = config("liberiser.config.modules.{$moduleName}.plugins", []);

        $modules = array_merge([$moduleName => config("liberiser.config.modules.{$moduleName}")], $modules);

        return $modules;
    }

    public static function getConfig($refresh = false): array
    {
        static $config = null;

        if ($config && ! $refresh) {
            return $config;
        }

        $moduleName = static::getModuleName();
        $defaultConfig = config("liberiser.{$moduleName}", []);
        $moduleConfig = config("liberiser.config.modules.{$moduleName}", []);

        $config = array_merge($defaultConfig, $moduleConfig);

        return $config;
    }

    public static function getPlugins(): array
    {
        static $plugins = null;

        if ($plugins) {
            return $plugins;
        }

        $moduleName = static::getModuleName();
        $plugins = config("liberiser.config.modules.{$moduleName}.plugins", []);

        return $plugins;
    }

    public static function getFields(?self $model = null): array
    {
        $fields = [];

        foreach (static::getModules() as $module => $overrides) {
            $moduleConfig = config("liberiser.{$module}");

            $fields = array_merge($fields, $moduleConfig['class']::getComponentFields($model));
        }

        return $fields;
    }

    public static function getColumns(): array
    {
        $columns = [];

        foreach (static::getModules() as $module => $settings) {
            $moduleConfig = config("liberiser.{$module}");

            if (! $moduleConfig) {
                throw new ConfigNotFoundException($module);
            }

            if (! class_exists($moduleConfig['class'])) {
                throw new ClassNotFoundException($moduleConfig['class']);
            }

            $columns = array_merge($columns, $moduleConfig['class']::getComponentColumns());
        }

        return $columns;
    }

    public static function relations(): array
    {
        $relations = [];

        $moduleName = static::getModuleName();
        $moduleConfig = config("liberiser.config.modules.{$moduleName}");
        $modules = [];

        $moduleRelations = $moduleConfig['autoload_relations'] ?? [];

        if ($moduleRelations && $moduleRelations === true) {
            foreach (static::getModules() as $module => $settings) {
                if ($module !== static::getModuleName()) {
                    $modules[] = $module;
                }
            }
        } elseif ($moduleRelations && is_array($moduleRelations)) {
            $modules = $moduleConfig['autoload_relations'];
        }

        foreach ($modules as $module) {
            $moduleConfig = config("liberiser.{$module}");

            if (! $moduleConfig) {
                throw new ConfigNotFoundException($module);
            }

            if (! class_exists($moduleConfig['class'])) {
                throw new ClassNotFoundException($moduleConfig['class']);
            }

            $relations = array_merge($relations, $moduleConfig['class']::getComponentRelations());
        }

        return $relations;
    }

    public static function getAllComponentRelations(): array
    {
        $relations = [];
        $modules = [];

        foreach (static::getModules() as $module => $settings) {
            if ($module !== static::getModuleName()) {
                $modules[] = $module;
            }
        }

        foreach ($modules as $module) {
            $moduleConfig = config("liberiser.{$module}");

            if (! $moduleConfig) {
                throw new ConfigNotFoundException($module);
            }

            if (! class_exists($moduleConfig['class'])) {
                throw new ClassNotFoundException($moduleConfig['class']);
            }

            $relations = array_merge($relations, $moduleConfig['class']::getComponentRelations());
        }

        return $relations;
    }

    public function getTranslatableFields(): array
    {
        $translatables = [];

        foreach (static::getModules() as $module => $moduleConfig) {
            $moduleConfig = config("liberiser.{$module}");

            if ($moduleConfig && isset($moduleConfig['class']::$translatableFields)) {
                $translatables = array_merge($translatables, $moduleConfig['class']::$translatableFields);
            }
        }

        return $translatables;
    }

    public static function getTableName(): string
    {
        return with(new static)->getTable();
    }

    public static function getActualClass(): string
    {
        return config(sprintf('liberiser.%s.class', static::getModuleName()), static::class);
    }

    public function save(array $options = [])
    {
        return parent::save($options);
    }

    /**
     * Handle file upload and DB storage for a file:
     * - on CREATE
     *     - stores the file at the destination path
     *     - generates a name
     *     - stores the full path in the DB;
     * - on UPDATE
     *     - if the value is null, deletes the file and sets null in the DB
     *     - if the value is different, stores the different file and updates DB value.
     *
     * @param string $value Value for that column sent from the input.
     * @param string $attribute_name Model attribute name (and column in the db).
     * @param string $disk Filesystem disk used to store files.
     * @param string $destination_path Path in disk where to store the files.
     * @param string $file_name The name of the uploaded file. Defaults to attribute name if null.
     * @throws Exception
     */
    public function uploadFileToDisk($value, $attribute_name, $disk, $destination_path, $file_name = null): void
    {
        if (! $file_name) {
            $file_name = $attribute_name;
        }

        $request = Request::instance();

        // if a new file is uploaded, delete the file from the disk
        if ($request->hasFile($file_name) &&
            $this->{$attribute_name} &&
            $this->{$attribute_name} != null) {
            Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // if the file input is empty, delete the file from the disk
        if (is_null($value) && $this->{$attribute_name} != null) {
            Storage::disk($disk)->delete($this->{$attribute_name});
            $this->attributes[$attribute_name] = null;
        }

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($file_name) && $request->file($file_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($file_name);
            $new_file_name = md5($file->getClientOriginalName().random_int(1, 9999).time()).'.'.$file->getClientOriginalExtension();

            // 2. Move the new file to the correct path
            $file_path = $file->storeAs($destination_path, $new_file_name, $disk);

            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = $file_path;
        }
    }

    /**
     * Dynamically handle calls to the class.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \BadMethodCallException
     */
    public function __call($method, $parameters)
    {
        if (! static::hasMacro($method) && in_array($method, ['increment', 'decrement'])) {
            return $this->$method(...$parameters);
        }

        if (static::hasMacro($method)) {
            $macro = static::$macros[$method];

            if ($macro instanceof Closure) {
                return call_user_func_array($macro->bindTo($this, static::class), $parameters);
            }

            return call_user_func_array($macro, $parameters);
        }

        return $this->forwardCallTo($this->newQuery(), $method, $parameters);
    }

    /**
     * Dynamically handle calls to the class.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \BadMethodCallException
     */
    public static function __callStatic($method, $parameters)
    {
        if (! static::hasMacro($method)) {
            return (new static)->$method(...$parameters);
        }

        if (static::$macros[$method] instanceof Closure) {
            return call_user_func_array(Closure::bind(static::$macros[$method], null, static::class), $parameters);
        }

        return call_user_func_array(static::$macros[$method], $parameters);
    }
}
