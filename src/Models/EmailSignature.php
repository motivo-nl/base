<?php

namespace Motivo\Liberiser\Base\Models;

use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;

class EmailSignature extends BaseModel
{
    use SoftDeletes;

    protected $table = 'liberiser_email_signatures';

    protected $fillable = ['name', 'signature'];

    public static $translatableFields = [];

    public function templates(): HasMany
    {
        return $this->hasMany(EmailTemplate::class, 'signature_id');
    }

    public static function getModuleName(): string
    {
        return 'email_signatures';
    }

    public static function getComponentFields(?BaseModel $model = null): array
    {
        return [
            [
                'name' => 'name',
                'label' => ucfirst(trans('Liberiser::emails/email-signature.fields.name')),
                'type' => 'text',
            ],
            [
                'name' => 'signature',
                'label' => ucfirst(trans('Liberiser::emails/email-signature.fields.signature')),
                'type' => 'wysiwyg',
            ],
        ];
    }

    public static function getComponentColumns(): array
    {
        return [
            [
                'name' => 'name',
                'label' => ucfirst(trans('Liberiser::emails/email-signature.fields.name')),
                'type' => 'text',
            ],
            [
                'name' => 'signature',
                'label' => ucfirst(trans('Liberiser::emails/email-signature.fields.signature')),
                'type' => 'wysiwyg',
            ],
        ];
    }

    public static function getComponentRelations(): array
    {
        return [];
    }

    public function getDisplayNameAttribute(): string
    {
        return $this->name;
    }

    /** @return mixed
     * @throws Exception
     */
    public function delete()
    {
        if ($this->templates()->count() > 0) {
            return false;
        }

        return parent::delete();
    }
}
