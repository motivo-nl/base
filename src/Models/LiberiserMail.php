<?php

namespace Motivo\Liberiser\Base\Models;

use Exception;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;
use Motivo\Liberiser\Base\Events\EmailCreated;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LiberiserMail extends BaseModel
{
    use SoftDeletes;

    public const STATUS_QUEUED = 0;

    public const STATUS_SENT = 1;

    public const STATUS_FAILED = 2;

    private const ATTACHMENT_FUNCTIONS = ['attach', 'attachFromStorage', 'attachFromStorageDisk', 'attachData'];

    private static $data = [];

    protected $table = 'liberiser_emails';

    protected $fillable = [];

    public function language(): BelongsTo
    {
        return $this->belongsTo(Language::class);
    }

    public function template(): BelongsTo
    {
        return $this->belongsTo(EmailTemplate::class, 'template_id')->withTrashed();
    }

    public static function getModuleName(): string
    {
        return 'liberiser_emails';
    }

    public static function getComponentFields(?BaseModel $model): array
    {
        return [];
    }

    public static function getComponentColumns(): array
    {
        return [
            [
                'name' => 'id',
                'type' => 'text',
                'label' => '#',
            ],
            [
                'type' => 'closure',
                'label' => '',
                'function' => (function ($entry) {
                    $color = 'orange';

                    switch ($entry->status) {
                        case self::STATUS_QUEUED:
                            $color = 'orange';

                            break;
                        case self::STATUS_SENT:
                            $color = 'green';

                            break;
                        case self::STATUS_FAILED:
                            $color = 'red';

                            break;
                    }

                    return "<i class=\"fa fa-circle\" style=\"color: {$color}\"></i>";
                }),
            ],
            [
                'label' => ucfirst(trans('Liberiser::emails.columns.sender')),
                'type' => 'closure',
                'function' => (function ($entry) {
                    return "{$entry->from_name} ({$entry->from_address})";
                }),
            ],
            [
                'label' => ucfirst(trans('Liberiser::emails.columns.receiver')),
                'type' => 'closure',
                'function' => (function ($entry) {
                    return "{$entry->to_name} ({$entry->to_address})";
                }),
            ],
            [
                'name' => 'subject',
                'type' => 'text',
                'label' => ucfirst(trans('Liberiser::emails.columns.subject')),
            ],
            [
                'label' => ucfirst(trans('Liberiser::emails.columns.created_at')),
                'type' => 'closure',
                'function' => (function ($entry) {
                    return $entry->created_at->format('d-m-Y H:i');
                }),
            ],
        ];
    }

    public static function getComponentRelations(): array
    {
        return [];
    }

    public function getDisplayNameAttribute(): string
    {
        return '';
    }

    public function encrypt(array $encryptData): void
    {
        $body = $this->body;
        $variables = $this->variables;

        foreach ($encryptData as $key => $value) {
            $body = str_replace($key, $value, $body);

            $jsonKey = json_encode($key);
            $jsonKey = substr($jsonKey, 1, strlen($jsonKey) - 2);

            $variables = str_replace($jsonKey, $value, $variables);
        }

        $this->body = $body;
        $this->variables = $variables;

        $this->save();
    }

    public static function SendTemplate(string $module, string $label, Language $language, string $senderReceiverName, string $senderReceiverAddress, array $variables = [], ?string $view = null, array $encrypt = []): ?self
    {
        $template = EmailTemplate::where('module', '=', $module)->where('label', '=', $label)->first();

        if (! $template) {
            return null;
        }

        if (! $view) {
            $view = 'Liberiser::email.template';
        }

        $fromName = $template->type === EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_SENDER ? $template->email_name : $senderReceiverName;
        $fromAddress = $template->type === EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_SENDER ? $template->email_address : $senderReceiverAddress;

        $toName = $template->type === EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_RECEIVER ? $template->email_name : $senderReceiverName;
        $toAddress = $template->type === EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_RECEIVER ? $template->email_address : $senderReceiverAddress;

        $viewExists = view()->exists($view);

        $model = new self();

        if ($viewExists) {
            $model->body = view($view, $variables)
                ->with('body', $template->getPropertyWithVariables('body', $variables, $language->shortcode))
                ->with('signature', $template->signature)
                ->render();
        }

        $model->language()->associate($language);
        $model->template()->associate($template);
        $model->from_name = $fromName;
        $model->from_address = $fromAddress;
        $model->to_name = $toName;
        $model->to_address = $toAddress;
        $model->subject = $template->getPropertyWithVariables('subject', $variables, $language->shortcode);
        $model->variables = json_encode($variables);
        $model->status = $viewExists ? self::STATUS_QUEUED : self::STATUS_FAILED;

        $model->save();

        if ($viewExists) {
            event(new EmailCreated($model, $encrypt, self::$data));
        }

        self::$data = [];

        return $model;
    }

    public static function Send(Language $language, string $subject, string $senderName, string $senderAddress, string $receiverName, string $receiverAddress, string $view, array $data, array $encrypt = []): ?self
    {
        $model = new self();

        $viewExists = view()->exists($view);

        if ($viewExists) {
            $model->body = view($view, $data)->render();
        }

        $model->language()->associate($language);
        $model->from_name = $senderName;
        $model->from_address = $senderAddress;
        $model->to_name = $receiverName;
        $model->to_address = $receiverAddress;
        $model->subject = $subject;
        $model->variables = json_encode($data);
        $model->status = $viewExists ? self::STATUS_QUEUED : self::STATUS_FAILED;

        $model->save();

        if ($viewExists) {
            event(new EmailCreated($model, $encrypt, self::$data));
        }

        self::$data = [];

        return $model;
    }

    public static function cc(string $emailAddress, string $name = null, string $type = 'cc'): void
    {
        self::$data[$type.'s'][] = [
            'email' => $emailAddress,
            'name' => $name,
        ];
    }

    public static function bcc(string $emailAddress, string $name = null): void
    {
        self::cc($emailAddress, $name, 'bcc');
    }

    public static function attachment(string $filePath, string $as = null, string $mimeType = null, string $function = 'attach', $disk = null): void
    {
        if (! in_array($function, self::ATTACHMENT_FUNCTIONS)) {
            throw new Exception('Attach function:'.$function.' is not valid');
        }

        self::$data['attachments'][] = [
            'filePath' => $filePath,
            'function' => $function,
            'as' => $as,
            'mimeType' => $mimeType,
            'disk' => $disk,
        ];
    }
}
