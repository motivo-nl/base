<?php

namespace Motivo\Liberiser\Base\Models;

use Motivo\Liberiser\Base\Liberiser;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EmailTemplate extends BaseModel
{
    use SoftDeletes;

    public const TYPE_CUSTOMER_CAN_SELECT_SENDER = 0;

    public const TYPE_CUSTOMER_CAN_SELECT_RECEIVER = 1;

    protected $table = 'liberiser_email_templates';

    protected $fillable = ['signature_id', 'type', 'module', 'label', 'variables', 'description', 'subject', 'body', 'email_name', 'email_address'];

    public static $translatableFields = ['description', 'subject', 'body'];

    public function signature(): BelongsTo
    {
        return $this->belongsTo(EmailSignature::class, 'signature_id')->withTrashed();
    }

    public function getPropertyWithVariables(string $property, array $variables, string $lang = null): string
    {
        $html = $this->{$property} ?? '';

        if ($lang) {
            $html = $this->getTranslation($property, $lang) ?? '';
        }

        foreach ($variables as $variable => $value) {
            $html = str_replace("%{$variable}%", $value, $html);
        }

        return $html;
    }

    public static function getModuleName(): string
    {
        return 'email_templates';
    }

    public static function getComponentFields(?BaseModel $model = null): array
    {
        $modules = [];

        foreach (Liberiser::getActiveModules() as $module) {
            $config = config("liberiser.{$module}");

            $packageNamespace = $config['package_namespace'] ?? 'fallback';

            $modules[$module] = ucfirst(trans("{$packageNamespace}::{$module}.name"));
        }

        return [
            [
                'name' => 'type',
                'label' => ucfirst(trans('Liberiser::emails/email-template.fields.type')),
                'type' => 'radio',
                'options' => [
                    self::TYPE_CUSTOMER_CAN_SELECT_SENDER => ucfirst(trans('Liberiser::emails/email-template.type.sender')),
                    self::TYPE_CUSTOMER_CAN_SELECT_RECEIVER => ucfirst(trans('Liberiser::emails/email-template.type.receiver')),
                ],
            ],
            [
                'name' => 'module',
                'label' => ucfirst(trans('Liberiser::emails/email-template.fields.module')),
                'type' => 'select_from_array',
                'options' => $modules,
                'default' => '',
            ],
            [
                'name' => 'label',
                'label' => ucfirst(trans('Liberiser::emails/email-template.fields.label')),
                'type' => 'text',
            ],
            [
                'name' => 'description',
                'type' => 'textarea',
                'label' => ucfirst(trans('Liberiser::emails/email-template.fields.description')),
            ],
            [
                'name' => 'variables',
                'label' => ucfirst(trans('Liberiser::emails/email-template.fields.variables')),
                'type' => 'table',
                'entity_singular' => 'variable',
                'columns' => [
                    'variable' => ucfirst(trans('Liberiser::emails/email-template.fields.variable')),
                    'desc' => ucfirst(trans('Liberiser::emails/email-template.fields.variable_description')),
                ],
            ],
        ];
    }

    public static function getManagerFields(self $template): array
    {
        return [
            [
                'name' => 'description',
                'label' => ucfirst(trans('Liberiser::emails/email-template.fields.description')),
                'type' => 'show_text',
                'callback' => function ($entry) {
                    return $entry->description;
                },
            ],
            [
                'name' => 'email_name',
                'label' => ucfirst(trans(
                    $template->type === self::TYPE_CUSTOMER_CAN_SELECT_SENDER ?
                        'Liberiser::emails/email-template.fields.sender_name' :
                        'Liberiser::emails/email-template.fields.receiver_name'
                )),
                'type' => 'text',
                'value' => $template->email_name,
            ],
            [
                'name' => 'email_address',
                'label' => ucfirst(trans(
                    $template->type === self::TYPE_CUSTOMER_CAN_SELECT_SENDER ?
                        'Liberiser::emails/email-template.fields.sender_address' :
                        'Liberiser::emails/email-template.fields.receiver_address'
                )),
                'type' => 'email',
                'value' => $template->email_address,
            ],
            [
                'name' => 'subject',
                'type' => 'text',
                'label' => ucfirst(trans('Liberiser::emails/email-template.fields.subject')),
                'value' => $template->subject,
            ],
            [
                'name' => 'body',
                'type' => 'wysiwyg_with_variables',
                'variables' => $template->variables_array,
                'label' => ucfirst(trans('Liberiser::emails/email-template.fields.content')),
                'value' => $template->body,
            ],
            [
                'name' => 'signature_id',
                'type' => 'select',
                'label' => ucfirst(trans('Liberiser::emails/email-template.fields.signature')),
                'entity' => 'signature',
                'attribute' => 'name',
                'model' => EmailSignature::class,
                'options' => (function ($query) {
                    return $query->orderBy('name', 'ASC')->get();
                }),
                'value' => $template->signature_id,
            ],
        ];
    }

    public static function getComponentColumns(): array
    {
        return [
            [
                'name' => 'module',
                'label' => ucfirst(trans('Liberiser::emails/email-template.columns.module')),
                'type' => 'text',
            ],
            [
                'name' => 'label',
                'label' => ucfirst(trans('Liberiser::emails/email-template.columns.label')),
                'type' => 'text',
            ],
            [
                'name' => 'subject',
                'label' => ucfirst(trans('Liberiser::emails/email-template.columns.subject')),
                'type' => 'text',
            ],
            [
                'name' => 'type',
                'label' => ucfirst(trans('Liberiser::emails/email-template.columns.send_to')),
                'type' => 'closure',
                'function' => (function ($entry) {
                    return ucfirst(trans(
                        $entry->type === self::TYPE_CUSTOMER_CAN_SELECT_SENDER ?
                            'Liberiser::emails/email-template.columns.sender' :
                            'Liberiser::emails/email-template.columns.receiver'
                        ));
                }),
            ],
            [
                'name' => 'description',
                'label' => ucfirst(trans('Liberiser::emails/email-template.columns.description')),
                'type' => 'text',
            ],
        ];
    }

    public static function getComponentRelations(): array
    {
        return [];
    }

    public function getDisplayNameAttribute(): string
    {
        return "{$this->module} {$this->label}";
    }

    public function getVariablesArrayAttribute(): array
    {
        if (empty($this->variables)) {
            return [];
        }

        return json_decode($this->variables, true);
    }
}
