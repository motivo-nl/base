<?php

namespace Motivo\Liberiser\Base\Models;

use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Mail\Mailer as MailerContract;

class LiberiserMailable extends Mailable
{
    /** @var LiberiserMail */
    public $mail;

    /** @var string */
    public $usedView;

    /** @var array */
    private $encrypt;

    /** @var array */
    private $data;

    public function __construct(LiberiserMail $mail, string $view, array $encrypt = [], array $data = [])
    {
        $this->mail = $mail;
        $this->usedView = $view;
        $this->encrypt = $encrypt;
        $this->data = $data;
    }

    public function build(): self
    {
        $mail = $this->from($this->mail->from_address, $this->mail->from_name)
            ->subject($this->mail->subject)
            ->view($this->usedView)
            ->with('html', $this->mail->body);

        $this->processData($mail);

        return $mail;
    }

    public function send(MailerContract $mailer): void
    {
        try {
            parent::send($mailer);
        } finally {
            $this->mail->encrypt($this->encrypt);
        }
    }

    private function processData($mail): void
    {
        if (isset($this->data['ccs'])) {
            $this->addCopiesToMail($mail, 'cc');
        }

        if (isset($this->data['bccs'])) {
            $this->addCopiesToMail($mail, 'bcc');
        }

        if (isset($this->data['attachments'])) {
            $this->addAttachmentsToMail($mail);
        }
    }

    private function addCopiesToMail($mail, string $type = 'cc'): void
    {
        $mail->$type($this->data[$type.'s']);
    }

    private function addAttachmentsToMail($mail): void
    {
        foreach ($this->data['attachments'] as $attachment) {
            if ($attachment['function'] === 'attachFromStorageDisk') {
                $mail->{$attachment['function']}($attachment['disk'], $attachment['filePath']);
            }

            // Hier moet uiteindelijk de implementatie komen voor het gebruik van de
            // attach, attachFromStorage en attachFromData functie van de Mail class.
        }
    }
}
