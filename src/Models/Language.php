<?php

namespace Motivo\Liberiser\Base\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Language.
 *
 * @property string $shortcode
 * @property string $label
 * @property string $label_localized
 * @property string $locale
 * @property bool   $available
 * @property bool   $active
 */
class Language extends Model
{
    protected $table = 'liberiser_languages';

    public $timestamps = false;

    protected $casts = [
        'available' => 'bool',
        'active' => 'bool',
    ];

    public static function available(): Collection
    {
        return static::where('available', '=', true)->get();
    }

    public static function active(): Collection
    {
        return static::where('active', '=', true)->get();
    }
}
