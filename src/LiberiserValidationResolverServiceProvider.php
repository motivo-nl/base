<?php

namespace Motivo\Liberiser\Base;

use ReflectionClass;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Motivo\Liberiser\Base\Validation\Validation;
use Motivo\Liberiser\Base\Validation\ValidationManager;
use Motivo\Liberiser\Base\Interfaces\ValidationInterface;
use Motivo\Liberiser\Base\Collections\ValidationCollection;

class LiberiserValidationResolverServiceProvider extends ServiceProvider
{
    public static $validationCollection;

    public function boot(): void
    {
        if (! empty(config('liberiser.config.validations'))) {
            $this->registerValidation();
        }

        /*
         * @param array $array
         * @param mixed $array
         * @param bool $searchMultiple
         */
        Arr::macro('in', function (array $array, $needle, $searchMultiple = false): bool {
            if ($searchMultiple) {
                $resultCount = 0;

                array_map(function ($item) use ($array, &$resultCount) {
                    $result = in_array($item, $array);

                    if ($result) {
                        $resultCount++;
                    }
                }, $needle);

                return $resultCount === count($needle);
            }

            return in_array($needle, $array);
        });
    }

    public function register(): void
    {
        self::$validationCollection = new ValidationCollection();

        $this->app->singleton('validation', ValidationManager::class);
    }

    private function registerValidation(): void
    {
        foreach (config('liberiser.config.validations') as $module => $validations) {
            array_map(function ($validationClass) use ($module) {
                $validationObject = new $validationClass;
                $reflectionClass = (new ReflectionClass($validationObject));

                if (! is_subclass_of($validationObject, ValidationInterface::class)
                    || $reflectionClass->isAbstract()
                    || $reflectionClass->isInterface()) {
                    return;
                }

                Validation::addValidation($module, [class_basename($validationObject) => $validationObject]);
            }, $validations);
        }
    }
}
