<?php

namespace Motivo\Liberiser\Base;

use Route;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Motivo\Liberiser\Base\Events\EmailCreated;
use Motivo\Liberiser\Base\Listeners\SendEmail;
use Motivo\Liberiser\Base\Models\EmailTemplate;
use Motivo\Liberiser\Base\Models\EmailSignature;
use Motivo\Liberiser\Base\Console\Commands\PublishUserCommand;
use Motivo\Liberiser\Base\Http\Middleware\PermissionsMiddleware;
use Motivo\Liberiser\Base\Console\Commands\LiberiserSeederCommand;
use Motivo\Liberiser\Base\Console\Commands\LiberiserUpdateCommand;
use Motivo\Liberiser\Base\Http\Middleware\RedirectToCorrectDomain;
use Motivo\Liberiser\Base\Console\Commands\LiberiserInstallCommand;
use Motivo\Liberiser\Base\Http\ViewComposers\AdminMenuItemsComposer;
use Motivo\Liberiser\Base\Console\Commands\LiberiserCreateUserCommand;
use Motivo\Liberiser\Base\Console\Commands\PublishBackpackConfigCommand;

class LiberiserServiceProvider extends ServiceProvider
{
    /** @var string */
    protected $publishablePrefix = 'liberiser';

    /** @var string */
    protected $packageNamespace = 'Liberiser';

    /** @var array */
    protected $consoleCommands = [
        LiberiserInstallCommand::class,
        LiberiserUpdateCommand::class,
        LiberiserCreateUserCommand::class,
        LiberiserSeederCommand::class,
        PublishUserCommand::class,
        PublishBackpackConfigCommand::class,
    ];

    public function boot(): void
    {
        $this->registerPublishing();
        $this->setupLiberiserMacros();

        $this->registerMiddlewareGroup($this->app->router);

        $this->loadTranslationsFrom(__DIR__.'/../publishables/resources/lang/base', $this->packageNamespace);

        $this->registerRoutes();

        $this->setMenuItems();

        $this->registerListeners();
    }

    public function register(): void
    {
        $this->app->register(LiberiserValidationResolverServiceProvider::class);

        $this->app->singleton('liberiser', LiberiserManager::class);

        $this->mergeConfigFrom(__DIR__.'/../config/main/config.php', 'liberiser.config');
        $this->mergeConfigFrom(__DIR__.'/../config/modules/users.php', 'liberiser.users');
        $this->mergeConfigFrom(__DIR__.'/../config/modules/email_templates.php', 'liberiser.email_templates');
        $this->mergeConfigFrom(__DIR__.'/../config/modules/email_signatures.php', 'liberiser.email_signatures');
        $this->mergeConfigFrom(__DIR__.'/../config/modules/liberiser_emails.php', 'liberiser.liberiser_emails');

        $this->commands($this->consoleCommands);

        $this->loadViewsFrom(__DIR__.'/../publishables/resources/views', $this->packageNamespace);

        View::composer('backpack::inc.sidebar_content', AdminMenuItemsComposer::class);

        $this->loadMigrationsFrom(__DIR__.'/Database/Migrations');

        require_once __DIR__.'/helpers.php';

        $this->app->make(Factory::class)->load(__DIR__.'/../tests/Factories');
    }

    public function registerRoutes(): void
    {
        $this->loadRoutesFrom(__DIR__.'/routes/account.php');
        $this->loadRoutesFrom(__DIR__.'/routes/admin.php');
    }

    private function registerPublishing(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/modules' => config_path('liberiser'),
            ], $this->publishablePrefix.':config');

            $this->publishes([
                __DIR__.'/../config/main' => config_path('liberiser'),
            ], $this->publishablePrefix.':config-main');

            $this->publishes([
                __DIR__.'/../publishables/Exceptions/Handler.php' => app_path('Exceptions/Handler.php'),
            ], $this->publishablePrefix.':exception-handler');

            $this->publishes([
                __DIR__.'/../publishables/resources/views/vendor/backpack/' => resource_path('views/vendor/backpack'),
            ], $this->publishablePrefix.':backpack');

            $this->publishes([
                __DIR__.'/../publishables/resources/lang/laravel/' => resource_path('lang/'),
                __DIR__.'/../publishables/resources/lang/vendor/' => resource_path('lang/vendor'),
                __DIR__.'/../publishables/resources/lang/base' => resource_path('lang/vendor/'.$this->packageNamespace),
            ], $this->publishablePrefix.':lang');

            $this->publishes([
                __DIR__.'/../publishables/resources/views/account' => resource_path('views/vendor/'.$this->packageNamespace.'/account'),
                __DIR__.'/../publishables/resources/views/email' => resource_path('views/vendor/'.$this->packageNamespace.'/email'),
            ], $this->publishablePrefix.':views');

            $this->publishes([
                __DIR__.'/../dist/public' => public_path(),
            ], $this->publishablePrefix.':public');
        }
    }

    private function setupLiberiserMacros(): void
    {
        foreach (config('liberiser.config.modules', []) as $moduleName => $moduleSettings) {
            if (isset($moduleSettings['plugins'])) {
                $module = config("liberiser.{$moduleName}");

                foreach ($moduleSettings['plugins'] as $pluginName => $pluginSettings) {
                    $plugin = config("liberiser.{$pluginName}");

                    if (isset($plugin['mixins'])) {
                        $module['class']::mixin(new $plugin['mixins']);
                    }
                }
            }
        }
    }

    public function registerMiddlewareGroup(Router $router): void
    {
        $backendKey = config('backpack.base.middleware_key');

        $middlewareClasses = [
            $backendKey => PermissionsMiddleware::class,
            'web' => RedirectToCorrectDomain::class,
        ];

        foreach ($middlewareClasses as $middlewareKey => $middlewareClass) {
            $router->pushMiddlewareToGroup($middlewareKey, $middlewareClass);
        }
    }

    public function registerListeners(): void
    {
        Event::listen(EmailCreated::class, SendEmail::class);
    }

    private function setMenuItems(): void
    {
        if (! $this->app->runningInConsole()) {
            $user = $this->getBaseMenuItem('fa-user', 'Liberiser::user.menu_label', route('crud.user.index'), 'users');
            Liberiser::setAdminMenuItems(BaseMenuItem::CATEGORY_MANAGEMENT, ucfirst(trans('Liberiser::common.management')), $user);

            $emailPrimary = $this->getBaseMenuItem('fa-envelope-square', 'Liberiser::emails.menu_title', route('crud.emails.index'), EmailTemplate::getModuleName());
            $emailSent = $this->getBaseMenuItem('fa-envelope-open', 'Liberiser::emails.menu_label', route('crud.emails.index'), EmailTemplate::getModuleName());
            $emailTemplate = $this->getBaseMenuItem('fa-file-contract', 'Liberiser::emails/email-template.menu_label', route('crud.email-templates.index'), EmailTemplate::getModuleName());
            $emailSignature = $this->getBaseMenuItem('fa-signature', 'Liberiser::emails/email-signature.menu_label', route('crud.email-signatures.index'), EmailSignature::getModuleName());
            $emailPrimary->setChilds($emailSent, $emailTemplate, $emailSignature);
            Liberiser::setAdminMenuItems(BaseMenuItem::CATEGORY_TOOLS, ucfirst(trans('Liberiser::common.tools')), $emailPrimary);
        }
    }

    private function getBaseMenuItem(string $icon, string $label, string $link, string $module): BaseMenuItem
    {
        $menu = new BaseMenuItem();

        $menu->setIcon($icon);
        $menu->setLabel(ucfirst(trans($label)));
        $menu->setLink($link);
        $menu->setModule($module);

        return $menu;
    }
}
