<?php

use Motivo\Liberiser\Base\Models\Permission;
use Motivo\Liberiser\Base\Models\EmailTemplate;

return [
    'class' => EmailTemplate::class,
    'required_permission_level' => Permission::ROLE_MANAGER,
    'default_from' => env('MAIL_DEFAULT_FROM', 'info@motivo.nl'),
    'default_to' => env('MAIL_DEFAULT_TO', 'info@motivo.nl'),
];
