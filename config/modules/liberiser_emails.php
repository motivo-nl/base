<?php

use Motivo\Liberiser\Base\Models\Permission;
use Motivo\Liberiser\Base\Models\LiberiserMail;

return [
    'class' => LiberiserMail::class,
    'required_permission_level' => Permission::ROLE_MANAGER,
];
