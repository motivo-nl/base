<?php

use Motivo\Liberiser\Base\Models\Permission;
use Motivo\Liberiser\Base\Models\EmailSignature;

return [
    'class' => EmailSignature::class,
    'required_permission_level' => Permission::ROLE_MANAGER,
];
