<?php

use App\User;
use Motivo\Liberiser\Base\Models\Permission;

return [
    'class' => User::class,
    'required_permission_level' => Permission::ROLE_MANAGER,
];
