<?php

return [
    'seeders' => [
        'Motivo\Liberiser\Base\Database\Seeds\LanguageSeeder',
    ],
    'from_email' => env('DEFAULT_FROM_EMAIL', 'test@example.com'),
];
