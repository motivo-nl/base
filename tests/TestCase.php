<?php

namespace Tests;

use Faker\Generator;
use Illuminate\Support\Facades\DB;
use Prologue\Alerts\Facades\Alert;
use Illuminate\Support\Facades\File;
use Backpack\Base\BaseServiceProvider;
use Backpack\CRUD\CrudServiceProvider;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Factory;
use Prologue\Alerts\AlertsServiceProvider;
use Motivo\Liberiser\Base\LiberiserManager;
use Orchestra\Testbench\Concerns\WithFactories;
use Orchestra\Testbench\TestCase as BaseTestCase;
use Motivo\Liberiser\Base\LiberiserServiceProvider;
use Illuminate\Database\Eloquent\Factory as DatabaseFactory;

class TestCase extends BaseTestCase
{
    use WithFactories;

    /** @var string */
    public static $database;

    /** @var string */
    public static $databaseUser;

    /** @var string */
    public static $databasePassword;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        if (! isset(static::$database) || ! isset(static::$databaseUser) || ! isset(static::$databasePassword)) {
            static::$database = env('LIBERISER_TESTING_DATABASE', null);
            static::$databaseUser = env('LIBERISER_TESTING_DATABASE_USER', null);
            static::$databasePassword = env('LIBERISER_TESTING_DATABASE_PASSWORD', null);

            if (file_exists(__DIR__.'/database.json')) {
                $file = file_get_contents(__DIR__.'/database.json');
                $json = json_decode($file);

                if (! isset(static::$database)) {
                    static::$database = isset($json->database) ? $json->database : 'liberiser';
                }

                if (! isset(static::$databaseUser)) {
                    static::$databaseUser = isset($json->user) ? $json->user : 'liberiser';
                }

                if (! isset(static::$databasePassword)) {
                    static::$databasePassword = isset($json->password) ? $json->password : 'root';
                }
            } else {
                if (! isset(static::$database)) {
                    static::$database = 'liberiser';
                }

                if (! isset(static::$databaseUser)) {
                    static::$databaseUser = 'liberiser';
                }

                if (! isset(static::$databasePassword)) {
                    static::$databasePassword = 'root';
                }
            }
        }

        parent::__construct($name, $data, $dataName);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->afterApplicationCreated(function () {
            $this->makeACleanSlate();
        });

        $this->beforeApplicationDestroyed(function () {
            $this->makeACleanSlate();
        });

        $this->withFactories(__DIR__.'/Factories');
    }

    protected function getPackageProviders($app): array
    {
        return [
            AlertsServiceProvider::class,
            BaseServiceProvider::class,
            CrudServiceProvider::class,
            LiberiserServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Alert' => Alert::class,
            'CRUD' => CrudServiceProvider::class,
            'liberiser' => LiberiserManager::class,
        ];
    }

    /**
     * Define environment setup.
     *
     * @param Application $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app): void
    {
        $config = $app->get('config');

        $config->set('app.key', 'base64:A15nxuPO3DAzhwP5M+K87b7wMm8KE5lDNWpRuyDvuxs=');

        $config->set('database.default', 'mysql');

        $config->set('database.connections.mysql.database', static::$database);
        $config->set('database.connections.mysql.username', static::$databaseUser);
        $config->set('database.connections.mysql.password', static::$databasePassword);
    }

    protected function makeACleanSlate(): void
    {
        File::delete(config_path('liberiser/config.php'));
        File::deleteDirectory(app_path('Exceptions'));
        File::delete(base_path('.env'));
        File::delete(base_path('.env.testing'));
        File::delete(base_path('.env.production'));
        File::delete(app_path('User.php'));
        File::deleteDirectory(config_path('backpack'));

        $this->truncateTestingDatabase();
    }

    protected function truncateTestingDatabase(): void
    {
        $this->artisan('migrate:fresh');
    }
}
