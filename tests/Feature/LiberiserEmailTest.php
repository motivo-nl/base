<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Facades\Mail;
use App\Http\Middleware\CheckIfAdmin;
use App\Http\Middleware\VerifyCsrfToken;
use Motivo\Liberiser\Base\Models\Language;
use Illuminate\Foundation\Testing\WithFaker;
use Motivo\Liberiser\Base\Models\EmailTemplate;
use Motivo\Liberiser\Base\Models\LiberiserMail;
use Motivo\Liberiser\Base\Models\LiberiserMailable;

class LiberiserEmailTest extends TestCase
{
    use WithFaker;

    /** @var string */
    private $locale = 'nl';

    /** @var Language */
    private $language;

    /** @var EmailTemplate */
    private $template;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed', ['--class' => 'Motivo\\Liberiser\\Base\\Database\\Seeds\\LanguageSeeder']);

        Mail::fake();

        $this->app->setLocale($this->locale);

        $this->language = Language::where('shortcode', '=', $this->locale)->first();

        $this->template = factory(EmailTemplate::class)->create(['type' => EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_SENDER]);

        $this->withoutMiddleware(VerifyCsrfToken::class);
        $this->withoutMiddleware(CheckIfAdmin::class);
    }

    /** @test */
    public function create_email_test(): void
    {
        $subject = $this->faker->sentence;
        $senderName = $this->faker->name;
        $senderEmail = $this->faker->email;
        $receiverName = $this->faker->name;
        $receiverEmail = $this->faker->email;
        $url = $this->faker->url;

        LiberiserMail::Send(
            $this->language,
            $subject,
            $senderName,
            $senderEmail,
            $receiverName,
            $receiverEmail,
            'Liberiser::email.user.registered',
            ['url' => $url]
        );

        $validationData = [
            'language_id' => $this->language->id,
            'template_id' => null,
            'from_name' => $senderName,
            'from_address' => $senderEmail,
            'to_name' => $receiverName,
            'to_address' => $receiverEmail,
            'subject' => $subject,
        ];

        $this->assertDatabaseHas('liberiser_emails', $validationData);
    }

    /** @test */
    public function create_template_email_test(): void
    {
        $module = $this->template->module;
        $label = $this->template->label;
        $senderReceiverName = 'test';
        $senderReceiverEmail = 'test@test.test';

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderReceiverName, $senderReceiverEmail);

        $validationData = [
            'language_id' => $this->language->id,
            'template_id' => $this->template->id,
            'to_name' => $senderReceiverName,
            'to_address' => $senderReceiverEmail,
            'subject' => $this->template->getTranslation('subject', $this->locale),
        ];

        $this->assertDatabaseHas('liberiser_emails', $validationData);
    }

    /** @test */
    public function create_template_email_with_cc_test(): void
    {
        $module = $this->template->module;
        $label = $this->template->label;
        $senderReceiverName = 'test';
        $senderReceiverEmail = 'test@test.test';

        LiberiserMail::cc('test@test1.test', 'Test Test');

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderReceiverName, $senderReceiverEmail);

        $validationData = [
            'language_id' => $this->language->id,
            'template_id' => $this->template->id,
            'to_name' => $senderReceiverName,
            'to_address' => $senderReceiverEmail,
            'subject' => $this->template->getTranslation('subject', $this->locale),
        ];

        $this->assertDatabaseHas('liberiser_emails', $validationData);
    }

    /** @test */
    public function create_template_email_with_bcc_test(): void
    {
        $module = $this->template->module;
        $label = $this->template->label;
        $senderReceiverName = 'test';
        $senderReceiverEmail = 'test@test.test';

        LiberiserMail::bcc('test@test1.test', 'Test Test');

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderReceiverName, $senderReceiverEmail);

        $validationData = [
            'language_id' => $this->language->id,
            'template_id' => $this->template->id,
            'to_name' => $senderReceiverName,
            'to_address' => $senderReceiverEmail,
            'subject' => $this->template->getTranslation('subject', $this->locale),
        ];

        $this->assertDatabaseHas('liberiser_emails', $validationData);
    }

    /** @test */
    public function create_template_email_with_attachment_test(): void
    {
        $module = $this->template->module;
        $label = $this->template->label;
        $senderReceiverName = 'test';
        $senderReceiverEmail = 'test@test.test';

        LiberiserMail::attachment('filename', null, null, 'attachFromStorageDisk', 'public');

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderReceiverName, $senderReceiverEmail);

        $validationData = [
            'language_id' => $this->language->id,
            'template_id' => $this->template->id,
            'to_name' => $senderReceiverName,
            'to_address' => $senderReceiverEmail,
            'subject' => $this->template->getTranslation('subject', $this->locale),
        ];

        $this->assertDatabaseHas('liberiser_emails', $validationData);
    }

    /** @test */
    public function email_sent_test(): void
    {
        $module = $this->template->module;
        $label = $this->template->label;
        $senderReceiverName = 'test';
        $senderReceiverEmail = 'test@test.test';

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderReceiverName, $senderReceiverEmail);

        Mail::assertSent(LiberiserMailable::class, 1);

        $validationData = [
            'language_id' => $this->language->id,
            'template_id' => $this->template->id,
            'subject' => $this->template->getTranslation('subject', $this->locale),
            'status' => LiberiserMail::STATUS_SENT,
        ];

        $this->assertDatabaseHas('liberiser_emails', $validationData);
    }

    /** @test */
    public function email_sent_with_cc_test(): void
    {
        $module = $this->template->module;
        $label = $this->template->label;
        $senderReceiverName = 'test';
        $senderReceiverEmail = 'test@test.test';

        LiberiserMail::cc('test@test1.test', 'Test Test');

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderReceiverName, $senderReceiverEmail);

        Mail::assertSent(LiberiserMailable::class, 1);

        $validationData = [
            'language_id' => $this->language->id,
            'template_id' => $this->template->id,
            'subject' => $this->template->getTranslation('subject', $this->locale),
            'status' => LiberiserMail::STATUS_SENT,
        ];

        $this->assertDatabaseHas('liberiser_emails', $validationData);
    }

    /** @test */
    public function email_sent_with_bcc_test(): void
    {
        $module = $this->template->module;
        $label = $this->template->label;
        $senderReceiverName = 'test';
        $senderReceiverEmail = 'test@test.test';

        LiberiserMail::bcc('test@test1.test', 'Test Test');

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderReceiverName, $senderReceiverEmail);

        Mail::assertSent(LiberiserMailable::class, 1);

        $validationData = [
            'language_id' => $this->language->id,
            'template_id' => $this->template->id,
            'subject' => $this->template->getTranslation('subject', $this->locale),
            'status' => LiberiserMail::STATUS_SENT,
        ];

        $this->assertDatabaseHas('liberiser_emails', $validationData);
    }

    /** @test */
    public function email_sent_with_attachment_test(): void
    {
        $module = $this->template->module;
        $label = $this->template->label;
        $senderReceiverName = 'test';
        $senderReceiverEmail = 'test@test.test';

        LiberiserMail::attachment('filename', null, null, 'attachFromStorageDisk', 'public');

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderReceiverName, $senderReceiverEmail);

        Mail::assertSent(LiberiserMailable::class, function ($mail) {
            $mail->build();

            if (count($mail->diskAttachments) !== 1) {
                return false;
            }

            return true;
        });

        $validationData = [
            'language_id' => $this->language->id,
            'template_id' => $this->template->id,
            'subject' => $this->template->getTranslation('subject', $this->locale),
            'status' => LiberiserMail::STATUS_SENT,
        ];

        $this->assertDatabaseHas('liberiser_emails', $validationData);
    }

    /** @test */
    public function template_variables_are_replaced_test(): void
    {
        $variables = [
            'var1' => 'variable 1',
            'var2' => 'variable 2',
        ];

        $template = factory(EmailTemplate::class)->create([
            'type' => EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_SENDER,
            'variables' => json_encode($variables),
            'body' => json_encode([
                $this->locale => 'This %var1% should not be equal to %var2%',
            ]),
        ]);

        $module = $template->module;
        $label = $template->label;
        $senderReceiverName = 'test';
        $senderReceiverEmail = 'test@test.test';

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderReceiverName, $senderReceiverEmail, $variables);

        Mail::assertSent(LiberiserMailable::class, function (LiberiserMailable $mail) {
            $this->assertContains('This variable 1 should not be equal to variable 2', $mail->mail->body);

            return true;
        });

        $validationData = [
            'language_id' => $this->language->id,
            'template_id' => $template->id,
            'subject' => $template->getTranslation('subject', $this->locale),
            'status' => LiberiserMail::STATUS_SENT,
        ];

        $this->assertDatabaseHas('liberiser_emails', $validationData);
    }

    /** @test */
    public function signature_is_added_to_template_email_test(): void
    {
        $signature = $this->template->signature->signature;
        $module = $this->template->module;
        $label = $this->template->label;
        $senderReceiverName = 'test';
        $senderReceiverEmail = 'test@test.test';

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderReceiverName, $senderReceiverEmail);

        Mail::assertSent(LiberiserMailable::class, function (LiberiserMailable $mail) use ($signature) {
            $this->assertContains($signature, $mail->mail->body);

            return true;
        });
    }

    /** @test */
    public function from_email_is_correctly_set_when_type_is_zero_test(): void
    {
        $sender = $this->template->email_address;
        $module = $this->template->module;
        $label = $this->template->label;
        $receiverName = 'test';
        $receiverEmail = 'test@test.test';

        LiberiserMail::SendTemplate($module, $label, $this->language, $receiverName, $receiverEmail);

        Mail::assertSent(LiberiserMailable::class, function (LiberiserMailable $mail) use ($sender, $receiverEmail) {
            $mail->build();

            return $mail->hasTo($receiverEmail) && $mail->hasFrom($sender);
        });
    }

    /** @test */
    public function from_email_is_correctly_set_when_type_is_one_test(): void
    {
        $template = factory(EmailTemplate::class)->create([
            'type' => EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_RECEIVER,
        ]);

        $receiver = $template->email_address;
        $module = $template->module;
        $label = $template->label;
        $senderName = 'test';
        $senderEmail = 'test@test.test';

        LiberiserMail::SendTemplate($module, $label, $this->language, $senderName, $senderEmail);

        Mail::assertSent(LiberiserMailable::class, function (LiberiserMailable $mail) use ($receiver, $senderEmail) {
            $mail->build();

            return $mail->hasTo($receiver) && $mail->hasFrom($senderEmail);
        });
    }
}
