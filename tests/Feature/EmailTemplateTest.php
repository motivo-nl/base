<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Http\Response;
use App\Http\Middleware\CheckIfAdmin;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Foundation\Testing\WithFaker;
use Motivo\Liberiser\Base\Models\Permission;
use Motivo\Liberiser\Base\Models\EmailTemplate;

class EmailTemplateTest extends TestCase
{
    use WithFaker;

    private $locale = 'nl';

    public function setUp(): void
    {
        parent::setUp();

        $this->be(new User([
            'id' => 1,
            'first_name' => 'User',
            'last_name' => 'Name',
            'role' => Permission::ROLE_ADMIN,
        ]));

        $this->app->setLocale($this->locale);

        $this->withoutMiddleware(VerifyCsrfToken::class);
        $this->withoutMiddleware(CheckIfAdmin::class);
    }

    /** @test */
    public function create_email_template_test(): void
    {
        $inputData = [
            'type' => EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_SENDER,
            'module' => EmailTemplate::getModuleName(),
            'label' => 'email_template_created',
            'description' => $this->faker->paragraph,
            'variables' => json_encode([
                'a' => 'b',
                'c' => 'd',
            ]),
        ];

        $this->post(route('crud.email-templates.store'), $inputData)->assertStatus(Response::HTTP_FOUND);

        $matchData = $inputData;
        $matchData['description'] = json_encode([
            $this->locale => $inputData['description'],
        ]);

        $this->assertDatabaseHas('liberiser_email_templates', $matchData);
    }

    /** @test */
    public function update_email_template_test(): void
    {
        $inputData = [
            'type' => EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_SENDER,
            'module' => EmailTemplate::getModuleName(),
            'label' => 'email_template_created',
            'description' => $this->faker->paragraph,
            'variables' => json_encode([
                'a' => 'b',
                'c' => 'd',
            ]),
        ];

        $updateData = [
            'id' => 1,
            'type' => EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_RECEIVER,
            'module' => EmailTemplate::getModuleName(),
            'label' => 'email_template_created',
            'description' => $this->faker->paragraph,
            'variables' => json_encode([
                'a' => 'b',
                'c' => 'd',
                'e' => 'f',
            ]),
        ];

        $this->post(route('crud.email-templates.store'), $inputData)->assertStatus(Response::HTTP_FOUND);
        $this->put(route('crud.email-templates.update', 1), $updateData)->assertStatus(Response::HTTP_FOUND);

        $matchInputData = $inputData;
        $matchInputData['description'] = json_encode([$this->locale => $inputData['description']]);

        $matchUpdateData = $updateData;
        $matchUpdateData['description'] = json_encode([$this->locale => $updateData['description']]);

        $this->assertDatabaseMissing('liberiser_email_templates', $matchInputData);
        $this->assertDatabaseHas('liberiser_email_templates', $matchUpdateData);
    }

    /** @test */
    public function update_email_template_as_manager_test(): void
    {
        $inputData = [
            'type' => EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_SENDER,
            'module' => EmailTemplate::getModuleName(),
            'label' => 'email_template_created',
            'description' => $this->faker->paragraph,
            'variables' => json_encode([
                'a' => 'b',
                'c' => 'd',
            ]),
        ];

        $updateData = [
            'id' => 1,
            'email_name' => $this->faker->name,
            'email_address' => $this->faker->email,
            'subject' => $this->faker->sentence,
            'body' => $this->faker->paragraphs(3, true),
            'signature_id' => null,
        ];

        $this->post(route('crud.email-templates.store'), $inputData)->assertStatus(Response::HTTP_FOUND);
        $this->put(route('crud.email-templates.update-manager', 1), $updateData)->assertStatus(Response::HTTP_FOUND);

        $matchData = array_merge($inputData, $updateData);
        $matchData['description'] = json_encode([$this->locale => $inputData['description']]);
        $matchData['subject'] = json_encode([$this->locale => $updateData['subject']]);
        $matchData['body'] = json_encode([$this->locale => $updateData['body']]);

        $this->assertDatabaseHas('liberiser_email_templates', $matchData);
    }

    /** @test */
    public function delete_email_template_test(): void
    {
        $inputData = [
            'type' => EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_SENDER,
            'module' => EmailTemplate::getModuleName(),
            'label' => 'email_template_created',
            'description' => $this->faker->paragraph,
            'variables' => json_encode([
                'a' => 'b',
                'c' => 'd',
            ]),
        ];

        $this->post(route('crud.email-templates.store'), $inputData)->assertStatus(Response::HTTP_FOUND);

        $matchData = $inputData;
        $matchData['description'] = json_encode([$this->locale => $inputData['description']]);

        $this->assertDatabaseHas('liberiser_email_templates', $matchData);

        $this->delete(route('crud.email-templates.destroy', 1))->assertOk();

        $this->assertSoftDeleted('liberiser_email_templates', $matchData);
    }
}
