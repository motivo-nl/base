<?php

namespace Tests\Feature;

use Mockery;
use App\User;
use Mockery\Mock;
use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use App\Http\Middleware\CheckIfAdmin;
use Illuminate\Support\Facades\Config;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Foundation\Testing\WithFaker;
use Motivo\Liberiser\Base\Models\Permission;

class PermissionTest extends TestCase
{
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('migrate');

        $this->withoutMiddleware(VerifyCsrfToken::class);
        $this->withoutMiddleware(CheckIfAdmin::class);
    }

    /** @test */
    public function a_super_admin_can_visit_every_route(): void
    {
        $user = factory(User::class)->create(['role' => Permission::ROLE_ADMIN]);

        Config::set('liberiser.users.required_permission_level', Permission::ROLE_ADMIN);
        User::getConfig(true);

        $this->actingAs($user)->get(route('crud.user.index'))->assertOk();
    }

    /** @test */
    public function an_admin_cannot_visit_a_super_admin_route(): void
    {
        $user = factory(User::class)->create(['role' => Permission::ROLE_MANAGER]);

        Config::set('liberiser.users.required_permission_level', Permission::ROLE_ADMIN);
        User::getConfig(true);

        $this->actingAs($user)->get(route('crud.user.index'))->assertForbidden();
    }

    /** @test */
    public function an_admin_can_visit_an_admin_route(): void
    {
        $user = factory(User::class)->create(['role' => Permission::ROLE_MANAGER]);

        Config::set('liberiser.users.required_permission_level', Permission::ROLE_MANAGER);
        User::getConfig(true);

        $this->actingAs($user)->get(route('crud.user.index'))->assertOk();
    }

    /** @test */
    public function a_content_manager_cannot_visit_an_admin_route(): void
    {
        $user = factory(User::class)->create(['role' => Permission::ROLE_CONTENT_MANAGER]);

        Config::set('liberiser.users.required_permission_level', Permission::ROLE_MANAGER);
        User::getConfig(true);

        $this->actingAs($user)->get(route('crud.user.index'))->assertForbidden();
    }

    /** @test */
    public function a_content_manager_cannot_visit_a_route_without_permission(): void
    {
        $user = factory(User::class)->create(['role' => Permission::ROLE_CONTENT_MANAGER]);

        Config::set('liberiser.users.required_permission_level', Permission::ROLE_CONTENT_MANAGER);
        User::getConfig(true);

        $this->actingAs($user)->get(route('crud.user.index'))->assertForbidden();
    }

    /** @test */
    public function a_content_manager_can_visit_a_route_with_permission(): void
    {
        /** @var User $user */
        $user = factory(User::class)->create(['role' => Permission::ROLE_CONTENT_MANAGER]);
        $user->permissions()->create(['module' => User::getModuleName()]);

        Config::set('liberiser.users.required_permission_level', Permission::ROLE_CONTENT_MANAGER);
        User::getConfig(true);

        $this->actingAs($user)->get(route('crud.user.index'))->assertOk();
    }
}
