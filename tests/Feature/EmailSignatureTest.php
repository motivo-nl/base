<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\Response;
use App\Http\Middleware\CheckIfAdmin;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Foundation\Testing\WithFaker;

class EmailSignatureTest extends TestCase
{
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();

        $this->withoutMiddleware(VerifyCsrfToken::class);
        $this->withoutMiddleware(CheckIfAdmin::class);
    }

    /** @test */
    public function create_email_signature_test(): void
    {
        $inputData = [
            'name' => $this->faker->name,
            'signature' => $this->faker->paragraph,
        ];

        $this->post(route('crud.email-signatures.store'), $inputData)->assertStatus(Response::HTTP_FOUND);

        $this->assertDatabaseHas('liberiser_email_signatures', $inputData);
    }

    /** @test */
    public function update_email_signature_test(): void
    {
        $inputData = [
            'name' => $this->faker->name,
            'signature' => $this->faker->paragraph,
        ];

        $this->post(route('crud.email-signatures.store'), $inputData)->assertStatus(Response::HTTP_FOUND);

        $updateData = [
            'id' => 1,
            'name' => $this->faker->name,
            'signature' => $this->faker->paragraph,
        ];

        $this->put(route('crud.email-signatures.update', 1), $updateData)->assertStatus(Response::HTTP_FOUND);

        $this->assertDatabaseMissing('liberiser_email_signatures', $inputData);
        $this->assertDatabaseHas('liberiser_email_signatures', $updateData);
    }

    /** @test */
    public function delete_email_signature_test(): void
    {
        $inputData = [
            'name' => $this->faker->name,
            'signature' => $this->faker->paragraph,
        ];

        $this->post(route('crud.email-signatures.store'), $inputData)->assertStatus(Response::HTTP_FOUND);

        $this->assertDatabaseHas('liberiser_email_signatures', $inputData);

        $this->delete(route('crud.email-signatures.destroy', 1))->assertOk();

        $this->assertSoftDeleted('liberiser_email_signatures', $inputData);
    }
}
