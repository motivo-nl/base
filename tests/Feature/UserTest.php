<?php

namespace Tests\Feature;

use Mockery;
use App\User;
use Mockery\Mock;
use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use App\Http\Middleware\CheckIfAdmin;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Foundation\Testing\WithFaker;

class UserTest extends TestCase
{
    use WithFaker;

    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('migrate');

        $this->user = factory(User::class)->create();

        $this->withoutMiddleware(VerifyCsrfToken::class);
        $this->withoutMiddleware(CheckIfAdmin::class);
    }

    /** @test */
    public function create_user_test(): void
    {
        $data = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->firstName,
            'email' => $this->faker->email,
            'role' => 0,
        ];

        $response = $this->actingAs($this->user)->post(route('crud.user.store'), $data);

        $this->assertDatabaseHas('users', $data);
    }

    /** @test */
    public function unsigned_url_cannot_set_password_for_new_user(): void
    {
        $newUser = factory(User::class)->create(['password' => null]);

        $this->post(route('liberiser.account.complete-registration', ['user' => $newUser]))->assertForbidden();
    }

    /** @test */
    public function signed_url_can_set_password_for_new_user(): void
    {
        $newUser = factory(User::class)->create(['password' => null]);

        $this->post(URL::signedRoute('liberiser.account.complete-registration', ['user' => $newUser]))->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function delete_user_test(): void
    {
        $newUser = factory(User::class)->create(['password' => null]);

        $response = $this->actingAs($this->user)->delete(route('crud.user.destroy', ['user' => $newUser]))->assertOk();
    }
}
