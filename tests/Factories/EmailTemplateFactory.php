<?php

use Faker\Generator as Faker;
use Motivo\Liberiser\Base\Models\EmailTemplate;
use Motivo\Liberiser\Base\Models\EmailSignature;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(EmailTemplate::class, function (Faker $faker) {
    return [
        'signature_id' => factory(EmailSignature::class)->create()->id,
        'type' => $faker->randomElement([EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_SENDER, EmailTemplate::TYPE_CUSTOMER_CAN_SELECT_RECEIVER]),
        'module' => $faker->word,
        'label' => $faker->word,
        'variables' => json_encode([
            $faker->word => $faker->word,
            $faker->word => $faker->word,
            $faker->word => $faker->word,
        ]),
        'description' => json_encode([
            'nl' => $faker->paragraph,
            'en' => $faker->paragraph,
        ]),
        'subject' => json_encode([
            'nl' => $faker->sentence,
            'en' => $faker->sentence,
        ]),
        'body' => json_encode([
            'nl' => $faker->paragraphs(5, true),
            'en' => $faker->paragraphs(5, true),
        ]),
        'email_name' => $faker->name,
        'email_address' => $faker->email,
    ];
});
