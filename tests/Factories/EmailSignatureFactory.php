<?php

use Faker\Generator as Faker;
use Motivo\Liberiser\Base\Models\EmailSignature;

/* @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(EmailSignature::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'signature' => $faker->paragraph,
    ];
});
