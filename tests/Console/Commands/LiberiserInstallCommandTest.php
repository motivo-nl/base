<?php

namespace Tests\Console\Commands;

use Tests\TestCase;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LiberiserInstallCommandTest extends TestCase
{
    /** @test */
    public function liberiserConfigFileIsPublishedByCommand(): void
    {
        $originalFile = __DIR__.'/../../Stubs/.env';
        file_put_contents(base_path('.env'), file_get_contents($originalFile));

        $this->artisan('liberiser:install')
            ->expectsQuestion('What\'s your sentry key', 'testing')
            ->expectsQuestion('For which environment should the key be set', 'testing');

        $this->assertTrue(File::exists(config_path('liberiser/config.php')));
    }

    /** @test */
    public function liberiserExceptionHandlerIsPublishedByCommand()
    {
        $originalFile = __DIR__.'/../../Stubs/.env';
        file_put_contents(base_path('.env'), file_get_contents($originalFile));

        $this->artisan('liberiser:install')
            ->expectsQuestion('What\'s your sentry key', 'testing')
            ->expectsQuestion('For which environment should the key be set', 'testing');

        $file = app_path('Exceptions/Handler.php');

        $this->assertTrue(File::exists($file));

        $this->assertSame(
            File::get(__DIR__.'/../../../publishables/Exceptions/Handler.php'),
            File::get($file)
        );
    }

    /** @test */
    public function liberiserCommandCreatesEnvironmentFileBasedOnQuestion()
    {
        $originalFile = __DIR__.'/../../Stubs/.env';
        file_put_contents(base_path('.env'), file_get_contents($originalFile));

        $environmentFile = '.env.testing';

        $this->artisan('liberiser:install')
            ->expectsQuestion('What\'s your sentry key', 'testing')
            ->expectsQuestion('For which environment should the key be set', 'testing')
            ->expectsOutput(sprintf('Created %s based on .env file', $environmentFile))
            ->expectsOutput(sprintf('Sentry key added for %s file', $environmentFile));
    }

    /** @test */
    public function liberiserCommandOverwritesEnvironmentFileBasedOnQuestion()
    {
        $originalFile = __DIR__.'/../../Stubs/.env.testing';
        file_put_contents(base_path('.env.testing'), file_get_contents($originalFile));

        $environmentFile = '.env.testing';

        $this->artisan('liberiser:install')
            ->expectsQuestion('What\'s your sentry key', 'woot')
            ->expectsQuestion('For which environment should the key be set', 'testing')
            ->expectsOutput(sprintf('Sentry key added for %s file', $environmentFile));

        $this->assertContains('woot', File::get(base_path('.env.testing')));
    }
}
