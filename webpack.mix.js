const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('publishables/resources/sass/liberiser-base.scss', 'dist/public/css')
    .sass('publishables/resources/sass/font-awesome.scss', 'dist/public/css')
    .less('publishables/resources/less/adminlte-skin-motivo.less', 'dist/public/css');
