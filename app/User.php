<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Motivo\Liberiser\Base\Liberiser;
use Illuminate\Notifications\Notifiable;
use Motivo\Liberiser\Base\Models\BaseModel;
use Motivo\Liberiser\Base\Models\Permission;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Notifiable;
    use SoftDeletes;
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;
    use MustVerifyEmail;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute(): string
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function permissions(): HasMany
    {
        return $this->hasMany(Permission::class);
    }

    public function hasPermission(string $module): bool
    {
        if ($this->role === Permission::ROLE_ADMIN) {
            return true;
        }

        $config = get_module_config($module);

        if (isset($config['required_permission_level']) && $this->role !== Permission::ROLE_CONTENT_MANAGER) {
            return $this->role <= $config['required_permission_level'];
        }

        return (bool) $this->permissions()->where('module', '=', $module)->first();
    }

    public static function getModuleName(): string
    {
        return 'users';
    }

    public static function getComponentFields(?BaseModel $model = null): array
    {
        $roles = [
            Permission::ROLE_MANAGER => ucfirst(trans('Liberiser::user.role.manager')),
            Permission::ROLE_CONTENT_MANAGER => ucfirst(trans('Liberiser::user.role.content_manager')),
        ];

        $user = backpack_user() ?? request()->user();

        if ($user->role === Permission::ROLE_ADMIN) {
            $roles[Permission::ROLE_ADMIN] = ucfirst(trans('Liberiser::user.role.admin'));
            ksort($roles);
        }

        $permissionableModules = collect(Liberiser::getPageModules())
            ->filter(function ($config, $module) {
                $permission = config("liberiser.{$module}.required_permission_level", Permission::ROLE_CONTENT_MANAGER);

                return $permission === Permission::ROLE_CONTENT_MANAGER;
            })
            ->toArray();

        $modules = [];

        foreach ($permissionableModules as $module => $label) {
            $modules[$module] = $label;
        }

        return [
            ['name' => 'first_name', 'type' => 'text', 'label' => ucfirst(trans('Liberiser::user.fields.first_name'))],
            ['name' => 'last_name', 'type' => 'text', 'label' => ucfirst(trans('Liberiser::user.fields.last_name'))],
            ['name' => 'email', 'type' => 'email', 'label' => ucfirst(trans('Liberiser::user.fields.email'))],
            ['name' => 'role', 'type' => 'user_permissions', 'label' => ucfirst(trans('Liberiser::user.fields.role')), 'roles' => $roles, 'modules' => $modules],
        ];
    }

    public static function getComponentColumns(): array
    {
        return [
            ['name' => 'first_name', 'type' => 'text', 'label' => ucfirst(trans('Liberiser::user.columns.first_name'))],
            ['name' => 'last_name', 'type' => 'text', 'label' => ucfirst(trans('Liberiser::user.columns.last_name'))],
            ['name' => 'email', 'type' => 'email', 'label' => ucfirst(trans('Liberiser::user.columns.email'))],
            [
                'name' => 'role_column',
                'label' => ucfirst(trans('Liberiser::user.columns.role')),
                'type' => 'closure',
                'function' => function ($user) {
                    return role_name($user) ?? '';
                },
            ],
        ];
    }

    public static function getComponentRelations(): array
    {
        return [];
    }

    public function getDisplayNameAttribute(): string
    {
        return $this->full_name;
    }
}
